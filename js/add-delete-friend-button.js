function addFriendButton(friendID, userID){
	jQuery.mobile.loading('show');
	console.log("Friend button pressed!");
	var newFriendRequestURL = 'http://shareatalent.com/api/add_friend.php?userid='+userID+'&new='+friendID;
	console.log('Your new friend request URL: '+newFriendRequestURL);
	jQuery.getJSON(newFriendRequestURL,function(data){
		console.log(data);
		jQuery("#member-message").html('<h3 style="color:red;">'+data.message+'</h3>').trigger("create");
		jQuery("#member-message").toggle();
		// delete the add friend button
		jQuery('#member-friend-button').html('').trigger("create");
		});// getJSON
		
jQuery.mobile.loading('hide');

	
}// addFriendButton

function deleteFriendButton(friendID, userID){
	jQuery.mobile.loading('show');
	console.log(" DELETE Friend button pressed!");
	var deleteFriendRequestURL = 'http://shareatalent.com/api/delete_friend.php?userid='+userID+'&new='+friendID;
	console.log('Your new friend request URL: '+deleteFriendRequestURL);
	jQuery.getJSON(deleteFriendRequestURL,function(data){
		console.log(data);
		jQuery("#member-message").html('<h3 style="color:red;">'+data.message+'</h3>').trigger("create");
		jQuery("#member-message").toggle();
		// delete the add friend button
		jQuery('#member-delete').html('').trigger("create");
		});// getJSON
		
jQuery.mobile.loading('hide');

	
}// deleteFriendButton

function acceptFriendButton(friendID, userID){
	jQuery.mobile.loading('show');
	console.log("Accept Friend button pressed!");
	var acceptFriendRequestURL = 'http://shareatalent.com/api/accept_friend.php?userid='+userID+'&new='+friendID;
	console.log('Your new friend request URL: '+acceptFriendRequestURL);
	jQuery.getJSON(acceptFriendRequestURL,function(data){
		console.log(data);
		jQuery("#member-message").html('<h3 style="color:red;">'+data.message+'</h3>');
		jQuery("#member-message").toggle();
		// delete the add friend button
		jQuery('#member-delete').html('').trigger("create");
		});// getJSON
		
jQuery.mobile.loading('hide');

	
}// acceptFriendButton