document.addEventListener('deviceready', childAccountFunctions, false);
document.addEventListener('deviceready', getChildAccountPage, false);

function childAccountFunctions(){
cl("Child Accounts Functions Ready!");
jQuery("a[id^='child-accounts-menu']").on("vclick",function(){
	cl("Child Accounts Menu clicked");
	getChildAccountPage();
});// end vclick function


jQuery("#create-child-account-button").on("vclick",function(){
	cl("Create Child Account button pressed.");
	jQuery("#create-new-child-form").slideDown( 500 ).css("display","block");
	var UserID = window.localStorage.getItem("satUserID");
	var UserEmail = window.localStorage.getItem("satUserEmail");
	jQuery("#child-account-user-email").val(UserEmail).trigger("refresh");
	jQuery("#child-account-parent-id").val(UserID);		
	}); // end open create child account div

jQuery("#child-account-button-submit").on("click",function(){
	var UserID 		= window.localStorage.getItem("satUserID");
	var email 		= window.localStorage.getItem("satUserEmail");
	var username	=	jQuery("#child-account-user-name").val();
	var first		=	jQuery("#child-account-first-name").val();
	var last		=	jQuery("#child-account-last-name").val();
	var zip			=	jQuery("#child-account-zip").val();
	var year		=	jQuery("#child-account-year").val();
	var month		=	jQuery("#child-account-month").val();
	var pass		=	jQuery("#child-account-pass").val();

	var createNewChildAccountURL = 'http://www.shareatalent.com/api/createuser/createChild_api/?username='+username+'&pass='+pass+'&email='+email+'&first='+first+'&last='+last+'&zip='+zip+'&year='+year+'&month='+month+'&parent='+UserID;
	cl(createNewChildAccountURL);
	jQuery.ajax({
				url: createNewChildAccountURL,
				success: function(results){
				var message = results.message;
				var status = results.status;
				if(status == "ok") {
				cl("Results: "+results.message);
				jQuery("#create-new-child-form").slideUp( 100 ).css("display","none");
				jQuery("#child-account-message").delay( 150 ).slideDown( 500 ).html('<h1 style="color:red;">'+message+'!!</h1>');
				jQuery("#child-account-message").delay( 2000 ).slideUp( 500 );
				jQuery("#create-child-account-errors").empty();
				getChildAccountPage();
				}
				else if (status == "error") {
					var message = results.message;
					if (jQuery.isPlainObject(message)){ 
						jQuery.each(message.errors,function(result, value){
							message_login = message.errors.existing_user_login;
							message_email = message.errors.existing_user_email;
							message = message_login+message_email;
							cl("Error from WP: "+message);
						});// end each
					jQuery("#create-child-account-errors").delay( 150 ).slideDown( 500 ).html('<h1 style="color:red;">'+message+'</h1>');					
					} // end if errors creating user
					else {
					cl(message);
					jQuery("#create-child-account-errors").delay( 150 ).slideDown( 500 ).html('<h1 style="color:red;">'+message+'</h1>');
						}// end else
					}
					}
					
					});
	}); // end submit the child account info

jQuery("button[id^='child-change-']").on("vclick",function(){
	childList = this.id.replace("child-change-","");
	childPassID = jQuery("#child-"+childList+"-id").text();
	newChildPass = jQuery("#child-pass-"+childList).val();	
	var changeChildPassURL = "http://www.shareatalent.com/api/userprofile/childAccountsChangePassword_api/?userid="+childPassID+"&pass="+newChildPass;
	cl("Password URL: "+changeChildPassURL);
	jQuery.getJSON(changeChildPassURL, function(passChanged){
		cl(passChanged.message);
		jQuery("#password-message-"+childList).css("display","block").slideDown( 500 ).html("<h2 style='color:red'>"+passChanged.message+"!</h2>").delay( 2000 ).slideUp( 500 );
		});
	
	});
}// end childAccountFunction()

function getChildAccountPage(){
	var UserID = window.localStorage.getItem("satUserID");
	var childAccountFetchURL = "http://www.shareatalent.com/api/userprofile/childAccounts_api/?userid="+UserID;
	cl(childAccountFetchURL);
	jQuery.ajax({
		url: childAccountFetchURL,
		success: function(results){
		var details = results.details;
		var status = results.status;
		cl(details)
				if(details){
				jQuery("#child-account-list").css("display","block");
				var n = 1;				
				jQuery.each(details,function(result, value){
					childDisplay 	= value.childDisplay;
					childID 		= value.childID;
					childLogin 		= value.childLogin;
					childActive		= value.active;
					cl("Child Details #"+n+": "+childDisplay+" | "+childID+" | "+childLogin+" | "+childActive);
					if(childID) {
						jQuery("#child-"+n).css("display","block");	
						jQuery("#child-name-"+n).text(childDisplay);
						jQuery("#child-login-"+n).text(childLogin);
						jQuery("#child-"+n+"-id").text(childID);
						if(childActive == 1) {
							jQuery("#child-disable-"+n).empty();
							jQuery("#child-disable-"+n).html('<a style="display:block;" data-role=button href="#" id="child-toggle-off-'+childID+'">Disable Child Account</a><br /><a style="display:none;" data-role=button href="#" id="child-toggle-onn-'+childID+'">Enable Child Account</a>').trigger("create").trigger("refresh");
							jQuery("#child-disable-"+n).trigger("create").trigger("refresh");							

							
							
							}
						if(childActive == 0) {
							jQuery("#child-disable-"+n).empty();
							jQuery("#child-disable-"+n).html('<a data-role=button href="#" id="child-toggle-onn-'+childID+'">Enable Child Account</a>');
							jQuery("#child-disable-"+n).trigger("create").trigger("refresh");
							}							
						}
					n++;
				});// end each
				childToggleOff();	
				childToggleOn();						
				}// end if child accounts
			}// end AJAX success function			
			});	// end AJAX		
	
}//getChildAccountPage

function childToggleOff(){
jQuery("a[id^='child-toggle-off']").on("vclick",function(){
	var getDivID 	= this.id;
	var childID 	= getDivID.replace("child-toggle-off-","");
	var state 		= 1;
	var getParentDivID = jQuery("#"+getDivID).closest("div").attr("id");
	var n = getParentDivID.replace("child-disable-","");
	cl(n);
	var disableChildURL = "http://www.shareatalent.com/api/userprofile/childAccountsToggle_api/?userid="+childID+"&state="+state;
	cl(disableChildURL);

	jQuery.getJSON(disableChildURL,function(){
		cl("Disable Clicked");
		jQuery("#child-toggle-off-"+childID).css("display","none");
		jQuery("#child-toggle-onn-"+childID).css("display","block");
		})// end getJSON
	childToggleOn();
	});// end vclick function
}

function childToggleOn(){
jQuery("a[id^='child-toggle-onn']").on("vclick",function(){
	var getDivID 	= this.id;
	var childID 	= getDivID.replace("child-toggle-onn-","");
	var state 		= 0;
	var getParentDivID = jQuery("#"+getDivID).closest("div").attr("id");
	var n = getParentDivID.replace("child-disable-","");
	cl(n);
	var enableChildURL = "http://www.shareatalent.com/api/userprofile/childAccountsToggle_api/?userid="+childID+"&state="+state;
	cl(enableChildURL);

	jQuery.getJSON(enableChildURL,function(){
		cl("Enable Clicked");
		jQuery("#child-toggle-onn-"+childID).css("display","none");
		jQuery("#child-toggle-off-"+childID).css("display","block");
		})// end getJSON
	childToggleOff();
	});// end vclick function
}	