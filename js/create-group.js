function navigateToInviteFriends() { if (jQuery('#GroupName').val() == '') {
       navigator.notification.alert(
  "Please Enter Group Name",
  "Share A Talent", 
  "Close")
    } else {
        window.location ='#invite-friend-page';
    } }

var GroupId="";

jQuery('#invite-friend').on("vclick",function($){
	//alert("click");
	
	var GroupName = encodeURIComponent(jQuery('#GroupName').val());
	var GroupDescription = encodeURIComponent(jQuery('#GroupDescription').val());
	var status = encodeURIComponent(jQuery("input[name=PrivacyOption]:checked").val());
	var GroupPermission=jQuery("input[name=GroupInvitation]:checked").val();	
	var UserID = window.localStorage.getItem("satUserID");
	var UserName = window.localStorage.getItem("satUserName");
	var CreateGroupURL ='http://www.shareatalent.com/api/test.php?creator_id='+UserID+'&group_name='+GroupName+'&description='+GroupDescription+'&status='+status+'&grouppermission='+GroupPermission;
	// Ajax for create group
	jQuery.ajax({
		url: CreateGroupURL,
		dataType: "text",
		data:{},
		crossDomain: true,
		success:
		function(json) {  
		//alert(json);
		GroupId = JSON.parse(json);		
		alert(GroupId);	
				//var imageURI = encodeURIComponent(jQuery("#group_avatar_img").attr('src'));
				//uploadAvatar(imageURI,json.group_id);
				jQuery('#GroupName').val("");
				jQuery('#GroupDescription').val("");
				
				
				
				
		}// end success
		}); // end Ajax



	
	if (UserID>0){
	var FriendsURL = 'http://www.shareatalent.com/api/buddypressread/friends_get_friends/?username='+UserName;
				cl("FriendsURL: "+FriendsURL);
				jQuery.getJSON(FriendsURL,
				function(FriendsList){
					var status = FriendsList.status;
					jQuery("#invite-friends-list").empty();
					if (status == 'error'){
							jQuery("#invite-friends-list").html('<li><a href="#search-page">No friends found. Go find some!</a></li>').trigger("create").listview("refresh");
						}
					else {
								jQuery.each(FriendsList.friends, function (result, value){	
								jQuery("#invite-friends-list").append('<li><input type="checkbox" user-id="'+result+'" name="checkbox-'+result+'" class="custom" id="memberId-'+result+'"/>'+value.display_name+'</li>');					
								jQuery("#invite-friends-list").trigger("create").listview("refresh");
							});// end e
							
						
					}// end else
				});
	}
	

	
});

jQuery("#add-to-group").on("vclick",function(){
	var UserID = window.localStorage.getItem("satUserID");	
	if (jQuery('.custom:checked').length) {
          var chkId = [];
          jQuery('.custom:checked').each(function () {
            chkId += jQuery(this).attr('user-id') + ",";
          });
          chkId = chkId.slice(0, -1);
         alert(chkId);
        }
        else {
          alert('Nothing Selected');
        }
		
		var InviteFriendGroupURL ='http://www.shareatalent.com/api/invite_friends_to_group.php?group_id='+GroupId+'&user_id='+chkId+'&inviter_id='+UserID;
	// Ajax for create group
	jQuery.ajax({
		url: InviteFriendGroupURL,
		dataType: "text",
		data:{},
		crossDomain: true,
		success:
		function(json) {         				
		}// end success
		}); // end Ajax
		
});//end create group

function takeAvatar(){
	event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.FILE_URI,
	  sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        jQuery("#group_avatar_img").attr('src',imgData).css('width','91px').css('height','91px');
		image_changed = true;
        if(image_changed) {
            console.log("Ajax function");
            uploadAvatar(imgData);
        }
	},
	function() {
	  //Ignore
	},
	options);

	return false;
}
             
function chooseAvatar(){
   event.preventDefault();
	if (!navigator.camera) {
	  alert("Camera API not supported", "Error");
	  return;
	}
	var options =   {   quality: 50,
	  destinationType: Camera.DestinationType.FILE_URI,
	  sourceType: 0,      // 0:Photo Library, 1=Camera, 2=Saved Album
	  encodingType: 0     // 0=JPG 1=PNG
	};

	navigator.camera.getPicture(
	function(imgData) {
        jQuery("#group_avatar_img").attr('src',imgData).css('width','91px').css('height','91px');
		image_changed = true;
        if(image_changed) {
            console.log("Ajax function");
            uploadAvatar(imgData);
        }
	},
	function() {
	  //Ignore
	},
	options);

	return false;
}

function uploadAvatar(imageURI) {
    
    var UserID         =   window.localStorage.getItem("satUserID");
	var options        =   new FileUploadOptions();
	options.fileKey    =   "file";
	options.fileName   =   UserID+"-bpfull.jpg";
	options.mimeType   =   "image/jpeg";    
    var serverUrl      =   "http://www.shareatalent.com/api/upload_group_avatar.php";  
    
    var params = {};
    params.value1 = UserID;
    params.value2 = imageURI;
    
	var params = {};
	options.params = params;
	var fileTransfer = new FileTransfer();
    fileTransfer.upload(imageURI, encodeURI(serverUrl), onUploadSuccess, onUploadFail, options, true);
   
}
function onUploadSuccess(r) {
	
	
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

function onUploadFail(error) {
	
    alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}

function onConfirm(buttonIndex) {
    if(buttonIndex > 0) {
        if(buttonIndex == 1) {
            chooseAvatar();
        } else if(buttonIndex == 2) {
            takeAvatar();
        }
    }
}

function trigger_upload(){
	
	navigator.notification.confirm(
        'Do you want to change, your profile picture ?. Touch elsewhere to dismiss.', // message
         onConfirm,                     // callback to invoke with index of button pressed
        'Change the profile image ?',   // title
        ['Choose pic.','Take pic.']     // buttonLabels
    );
}