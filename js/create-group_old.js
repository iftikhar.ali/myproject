
document.addEventListener('deviceready', newfriendsgroup, false);
var GroupName="";
var GroupId="";
function newfriendsgroup(){
cl("group Friends Page Ready!");
jQuery("a[id^='create-group-menu']").on("vclick",function($) 
{


var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	
	if (UserID>0) 
	{	
		GroupName=prompt("Group Name");
		var CreateGroupURL ='http://www.shareatalent.com/api/create_group.php/?satUserID='+UserID;
		var GroupDetail = {"satUserID": UserID,
						   "groupName": GroupName,
						   "Description":'test ellipsonic group',
						   "news":'whatever',
						   "status":'public',
						   "is_invitation_only":1,
						   "enable_wire": 1,
						   "enable_forum": 1,
						   "enable_photos": 1,
						   "photos_admin_only" :1,
						   "avatar_thumb" :'some kind of path',
						   "avatar_full" :'some kind of path'};
		// Ajax for create group
		jQuery.ajax({
			url: CreateGroupURL,
			dataType: "text",
			data: GroupDetail,
			type: 'POST',
			crossDomain: true,
			async: false,
			success:
			function(json) {
				jQuery("#group-name-heading").text(GroupName);
				//alert(JSON.parse(json).id);
				GroupId =JSON.parse(json).id;
				var FriendsURL = 'http://www.shareatalent.com/api/buddypressread/friends_get_friends/?username='+UserName;
				cl("FriendsURL: "+FriendsURL);
				jQuery.getJSON(FriendsURL,
				function(FriendsList){
					var status = FriendsList.status;
					jQuery("#group-friends-list").empty();
					if (status == 'error'){
							jQuery("#group-friends-list").html('<li><a href="#search-page">No friends found. Go find some!</a></li>').trigger("create").listview("refresh");
						}
					else {
								jQuery.each(FriendsList.friends, function (result, value){	
								jQuery("#group-friends-list").append('<li><input type="checkbox" user-id="'+result+'" name="checkbox-'+result+'" class="custom" id="memberId-'+result+'"/>'+value.display_name+'</li>');					
								jQuery("#group-friends-list").trigger("create").listview("refresh");
							});// end e
							
						
					}// end else
				});
			}// end success
			}); // end Ajax
	
	}// end if actual user
	}); // end function

} // end function?

// create group
jQuery("#add-group-member-button").on("vclick",function(){
	var UserID = window.localStorage.getItem("satUserID");	
	if (jQuery('.custom:checked').length) {
          var chkId = [];
          jQuery('.custom:checked').each(function () {
            chkId += jQuery(this).attr('user-id') + ",";
          });
          chkId = chkId.slice(0, -1);
          //alert(chkId);
        }
        else {
          alert('Nothing Selected');
        }
	
		var InviteFriendURL ='http://www.shareatalent.com/api/invite_friends_to_group.php/?group_id='+GroupId+'&user_id='+chkId+'&inviter_id='+UserID;

	// Ajax for invite friends 
	jQuery.ajax({
		url: InviteFriendURL,
		dataType: "text",
		data:{},
		crossDomain: true,
		success:
		function(json) {         			
		//alert(json);
		//alert("Joined");
		}// end success
		}); // end Ajax
	
});// end create group