function endorse(userID, friendID, talentID, talentName, endorsedName){
	
	var values = userID+' '+friendID+' '+talentID+' '+talentName+' '+endorsedName;
	console.log("Endorsing: "+ values);
	var promptName = "Are you sure you want to endorse "+endorsedName+" for "+talentName+"?";
	navigator.notification.confirm(promptName, function(button){
		if(button == 1){
			confirmEndorse(userID, friendID, talentID, talentName, endorsedName)
			}
		},"ShareATalent",['Yes','No'])	
	}//endorse
	
function unendorse(userID, friendID, talentID, talentName, endorsedName){
	
	var values = userID+' '+friendID+' '+talentID+' '+talentName+' '+endorsedName;
	console.log("Unendorsing: "+ values);
	var promptName = "Are you sure you want to withdraw your endorsement of "+endorsedName+" for "+talentName+"?";
	navigator.notification.confirm(promptName, function(button){
		if(button == 1){
			confirmUnendorse(userID, friendID, talentID, talentName, endorsedName)
		}
		},"ShareATalent",['Yes','No'])
	
	}//endorse
	
function confirmEndorse(userID, friendID, talentID, talentName, endorsedName){
		jQuery("#member-endorse-button").html('<button onClick="unendorse('+friendID+', '+userID+', '+talentID+', \''+talentName+'\', \''+endorsedName+'\')" id="endorse-friend-button-'+friendID+'">Endorsed <i style="color:green;" class="fa fa-check"></i></button>').trigger('create');
		var url = 'http://www.shareatalent.com/api/endorse_user.php?friend='+friendID+'&user_id='+userID+'&talent_id='+talentID+'&type=1'
		console.log(url);
		jQuery.getJSON(url,function(d){console.log(d)})
		};

function confirmUnendorse(userID, friendID, talentID, talentName, endorsedName){
		var values = userID+' '+friendID+' '+talentID+' '+talentName+' '+endorsedName;
	console.log("Unendorsing: "+ values);
		jQuery("#member-endorse-button").html('<button onClick="endorse('+friendID+', '+userID+', '+talentID+', \''+talentName+'\', \''+endorsedName+'\')" id="endorse-friend-button-'+friendID+'">Endorse</button>').trigger('create');
		var url = 'http://www.shareatalent.com/api/endorse_user.php?friend='+friendID+'&user_id='+userID+'&talent_id='+talentID+'&type=0'
		console.log(url);
		jQuery.getJSON(url,function(d){console.log(d)})
		};
		
/* 
$endorser_id	=	$_GET['user_id'];
$talent_id		=	$_GET['talent_id'];
$endorsed_user	=	$_GET['friend'];
$type			=	$_GET['type'];
*/