document.addEventListener('deviceready', getGroupsPageFunction, false);

function getGroupsPageFunction(){
	cl("Groups Page Functions Ready!");
jQuery("a[id^='groups-page-menu'], #main-profile-groups-link").on("vclick",function() 
{
jQuery("#groups-content-list").empty();
var UserID = window.localStorage.getItem("satUserID");	
var UserName = window.localStorage.getItem("satUserName");	

cl("UserID: "+UserID)
cl("UserName: "+UserName)
	if (UserID>0) 
	{		
		var GroupsURL = 'http://www.shareatalent.com/api/buddypressread/groups_get_groups/?username='+UserName;
			cl("GroupsURL: "+GroupsURL);
			jQuery.getJSON(GroupsURL,
			function(GroupsList){
					var status = GroupsList.status;
					if (status == 'error'){jQuery("#groups-content-list").html('<li>No groups found.</li>').trigger("create").listview("refresh");}
					else {
					cl("GroupsList: "+GroupsList.groups);
					jQuery.each(GroupsList.groups, function (result, value){	
						jQuery("#groups-content-list").append(
						//'<option value="'+value.category+'">'+value.category+'</option>');
						'<li><a href="http://www.shareatalent.com/groups/'+value.slug+'/">'+value.name+'<meta value="'+result+'" /></a></li>');
						jQuery("#groups-content-list").trigger("create").listview("refresh");		
					}); // end each
					}// end else
				});
	
	}// end if actual user
	}); // end function

}; // end function