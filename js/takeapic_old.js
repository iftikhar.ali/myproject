document.addEventListener('deviceready', MainUploaderFunction, false);
function MainUploaderFunction() { 
cl("Ready for Uploading!");
var UserID = window.localStorage.getItem("satUserID");
	jQuery("#post-update-button").on("click",function(){
		cl("Update Button clicked!");
		
		jQuery("#cancel-post-update-container").css("display", "block");
		jQuery("#post-update-button-container").css("display", "none");

		jQuery("#cancel-post-update").on("click",function(){
			jQuery("#make-a-post").slideUp("slow");
			jQuery("#cancel-post-update-container").css("display", "none");
			jQuery("#post-update-button-container").css("display", "block");
			});
		jQuery("#make-a-post").slideDown("slow");
		// Post button gets clicked
		jQuery("#post-submit-button").on("click",function( event ) {
			event.preventDefault();
		 	var d = new Date();
			var month 	= d.getMonth() +1; // Returns the month
			var year 	= d.getFullYear(); // Returns the year
			var uploadURL = 'http://www.shareatalent.com/api/usersettings/postUpdate/';
			var uploadURL2= 'http://www.shareatalent.com/wp-content/uploads/rtMedia/users/'+UserID+'/'+year+'/'+month+'/';
	// 
	var fileInput = document.getElementById('file-to-upload');
	var file = fileInput.files[0];
	cl("HTML5 upload: _____");
	cl(file);
	cl("___________________");
	cl(uploadURL2);	
	var content = jQuery("#post-update-content").val();
	if(content) {file.postContent = content}else{file.postContent = "blank"};
	file.postContent = jQuery("#post-update-content").val();
	file.userid = UserID,
	cl("Fise name "+file.name); // "my-holiday-photo.jpg"
	cl("File size: "+file.size); // 1282632
	cl("File type: "+file.type); // image/jpeg
	cl("UserID: "+file.userid); // UserID
	cl("Content: "+file.postContent); // post message			
	
	jQuery.ajax({				
		url: uploadURL,
		timeout: 10000,
		type: 'POST',			
		beforeSend: function(xhr){
			jQuery.mobile.showPageLoadingMsg(); 
			xhr.setRequestHeader('X-File-Name', file.name);
			xhr.setRequestHeader('X-USER-ID', UserID);
			xhr.setRequestHeader("Content-Type", "application/octet-stream");
			cl("XHR Header set in beforeSend");
			},			
		data: file,
		crossDomain: true,
		success: function(data){
			cl("File Submitted!");
			jQuery.mobile.hidePageLoadingMsg();			
			jQuery("#post-update-messages").slideDown("slow").html('<h3 style="color:red;">Posted!</h3>');
			jQuery("#post-update-messages").delay(2000).slideUp("slow");
			jQuery("#make-a-post").slideUp("fast");
			jQuery("#cancel-post-update-container").css("display", "none");
			jQuery("#post-update-button-container").css("display", "block");
			jQuery("#post-update-content").val('');
			jQuery("#file-to-upload").val('');
			cl("-------");
			cl(data);
			cl("-------");
			cl("Status: "+data.status);
			cl("Debug: "+data.debug);
			},// end success function
		cache: false,
		contentType: false,
		processData: false,
		complete: function(event, xhr){
			cl("Complete");
			//cl(event.statusText)		
			},// complete
		error: function(jqXHR, textStatus, errorThrown)
			{
				cl("Error f1: "+errorThrown);
				cl(jqXHR);
				jQuery.mobile.hidePageLoadingMsg();	
			}
		});// end ajax
				
			});
		});
		
}// end MainUploaderFunction