document.addEventListener('deviceready', MainUploaderFunction, false);

function MainUploaderFunction() { 
var files = [];
cl("UserAgent: "+navigator.userAgent)
if( /Android|iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
	cl("This ain't a mobile device!!");	
	//jQuery("#file-to-upload-container").css("display","none");	
}
cl("Ready for Uploading! (updatepost.js)");
pictureSource=navigator.camera.PictureSourceType;
destinationType=navigator.camera.DestinationType;
var UserID = window.localStorage.getItem("satUserID");
cl("Waiting for update button to be pressed.");
	jQuery("#post-update-button").on("click",function(){
		console.log("Update Button clicked!");
		var platform = device.platform;
       console.log("platform: "+platform);
       //alert(platform);
	jQuery("#cancel-post-update-container").delay(200).slideDown("fast");
	jQuery("#post-update-button-container").css("display", "none");

	jQuery("#cancel-post-update").on("click",function(){
		jQuery("#make-a-post").slideUp("fast");
		jQuery("#cancel-post-update-container").css("display", "none");
		jQuery("#post-update-button-container").delay(200).slideDown("fast");
		window.localStorage.setItem("SATempImage", null);
		imageData = '';
		largeImage.style.display = 'none';
		largeImage.src = imageData;
		smallImage.style.display = 'none';
		smallImage.src = imageData;
		jQuery("#post-update-content").val('');
		jQuery("#audio-play-button-container").slideUp('fast');
		jQuery("#stop-playing-audio-container").slideUp('fast');
		jQuery("#start-recording-audio-container").slideDown('fast');
		});// end cancel
	jQuery("#make-a-post").slideDown("fast");
	// Post button gets clicked
	jQuery("#post-submit-button").on("click",function( event ) {
		event.preventDefault();
		var d = new Date();
		var month 	= d.getMonth() +1; // Returns the month
		var year 	= d.getFullYear(); // Returns the year
		var uploadURL = 'http://www.shareatalent.com/api/usersettings/postUpdate/';
		var uploadURL2= 'http://www.shareatalent.com/wp-content/uploads/rtMedia/users/'+UserID+'/'+year+'/'+month+'/';
	});// end line 5 function
	});// end post-submit-button


jQuery("#camera-button").on("vclick",function(){ 
cl("Camera button pressed");
capturePhoto();
});// on click


function takeapic(){
navigator.camera.getPicture(
                uploadPhoto,
                function(message) { alert('get picture failed'); },
                {
                    quality         : 50,
                    destinationType : navigator.camera.DestinationType.FILE_URI,
                    sourceType      : navigator.camera.PictureSourceType.PHOTOLIBRARY
                }
            );

function uploadPhoto(imageURI) {
	cl("uploadPhoto called");
	var options = new FileUploadOptions();
	options.fileKey="file";
	options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
	options.mimeType="image/jpeg";
	var UserID = window.localStorage.getItem("satUserID");	
	var params = {};
	params.userid = UserID;
	params.value2 = "param";
	cl("Is this used? Line 73 - updatepost.js");
	options.params = params;

	var ft = new FileTransfer();
	ft.upload(imageURI, encodeURI("http://www.shareatalent.com/api/usersettings/postUpdate/"), win, fail, options);
}


}// end on click

// Look for upload button change
jQuery("#file-to-upload").on("change",function(event){	
	jQuery.each(event.target.files, function(index, file) {
    var reader = new FileReader();
    reader.onload = function(event) {  
      object = {};
      object.filename = file.name;
      object.data = event.target.result;
      files.push(object);
	  console.log("object.data:")
	  console.log(object.data);
	  window.localStorage.setItem("satImageObject",object.data);
    };  
    reader.readAsDataURL(file);
  });
})// input change

cl("Ready for submit button");
jQuery("#post-submit-button").on("vclick",function(){
	cl("File to Upload click");
	jQuery.mobile.loading('show');
//	imageData = window.localStorage.getItem("SATempImage2");
	imageData =  window.localStorage.getItem("satImageObject");
	audioData = window.localStorage.getItem("sat_recording_name");
	console.log("imageData: "+imageData);
	console.log("audioData: "+audioData);
	if(imageData != null){cl("Has an image! "+imageData)}else{cl("No image selected");}
	if(audioData != null){cl("Has audio! "+audioData)}else{cl("No audio recorded");}
	postContent = jQuery("#post-update-content").val();
	if(!postContent){navigator.notification.alert(
		"Wait! You have to put something in the Update Status box!",
		function(){jQuery.mobile.loading('hide');},
		"Share A Talent", 
		"Close")}// end if no post content
	else{
		cl("postContent: "+postContent);
		
	
// build the URL
var UserID = window.localStorage.getItem("satUserID");	
var d = new Date();
var month 		= d.getMonth() +1; // Returns the month
var year 		= d.getFullYear(); // Returns the year
var uploadURL 	= 'http://www.shareatalent.com/api/usersettings/postUpdate/';
var folder		= 'http://www.shareatalent.com/wp-content/uploads/rtMedia/users/'+UserID+'/'+year+'/'+month+'/';
cl("folder: "+folder);
var uri = encodeURI(uploadURL);

cl("Start building options");

//var options = new FileUploadOptions();
//options.fileKey="file";

	// if imagedata
	if(imageData){
	//options.fileName=imageData.substr(imageData.lastIndexOf('/')+1);
	//options.mimeType="text/plain";
	cl("options (image):");
	//console.log(options);
	/*
	var params = {};
	params.userid 	= UserID;
	params.filename = imageData.substr(imageData.lastIndexOf('/')+1);
	params.folder 	= folder;
	params.postcon 	= postContent;
	cl("file size (image): "+imageData.size);
	cl("params (image):");
	console.log(params);

	//options.params = params;
	*/
	/******************************************/
console.log("New Upload Process: ")	
var passToken = window.localStorage.getItem("satpass");
var passUserName = window.localStorage.getItem("satUserName");
var tokenURL = 'http://www.shareatalent.com/wp-admin/admin-ajax.php';
var tokenData = {
	action		: "rtmedia_api",
	method		: "wp_login",
	username  	: passUserName,
	password  	: passToken
	};
jQuery.post(tokenURL,tokenData,function(t){
	console.log("Token created");
    tp = JSON.parse(t);
	console.log("Parsed Token:"+tp.data.access_token);
    token = tp.data.access_token;
	window.localStorage.setItem("satToken",token);
	console.log("SAtToken: ");
	console.log(tp.data.access_token)
	console.log("--------------");
	sendFiletoRTMedia();
	}).fail(function() {
    console.log("Unable to get token");
  });	
  
function sendFiletoRTMedia(){	
	var uploadToken = window.localStorage.getItem("satToken");
	var imageDataPre = window.localStorage.getItem("satImageObject");
	var imageData = imageDataPre.replace('data:image/jpeg;base64,','');
	var imageTitle = jQuery("#post-update-content").val();
	console.log(imageTitle);	
	console.log(imageData);	
	var uploadData = {
	action 			: "rtmedia_api",
	method			: "rtmedia_upload_media",
	token 			: uploadToken,
	rtmedia_file 	: imageData,
	title 			: imageTitle,
	context 		: "profile",
    image_type      : "jpeg"
}
console.log("uploadData:");
console.log(uploadData);
console.log("----------------");
	jQuery.post('http://www.shareatalent.com/wp-admin/admin-ajax.php',uploadData,function(u){
		console.log(u);
		});

	var ft = new FileTransfer();
	ft.upload(imageData, uri, win, fail, options);
	
	//}// end if imageData
}// sendFiletoRTMedia
	


// if audioData
audioRecorded = '';
if(audioData && audioRecorded){
options.fileName=audioData.substr(audioData.lastIndexOf('/')+1);
options.mimeType="audio";
cl("options (audio):");
console.log(options);

var params = {};
params.userid 	= UserID;
params.filename = audioData.substr(audioData.lastIndexOf('/')+1);
params.folder 	= folder;
params.postcon 	= postContent;
cl("file size (audio): "+audioData.size);
cl("params (audio):");
console.log(params);

options.params = params;

var ft = new FileTransfer();
ft.upload(audioData, uri, win, fail, options);
}// end if audioData

// Upload the file 
function win(r) {
    cl("Code = " + r.responseCode);
    cl("Response = " + r.response);
    cl("Sent = " + r.bytesSent);
	jQuery.mobile.loading('hide');
	// from cancel
	jQuery("#make-a-post").slideUp("fast");
	jQuery("#cancel-post-update-container").css("display", "none");
	jQuery("#post-update-button-container").css("display", "block");
	window.localStorage.setItem("SATempImage", null);
	imageData = '';
	var largeImage = document.getElementById('largeImage');
	largeImage.style.display = 'none';
	largeImage.src = imageData;
	smallImage.style.display = 'none';
	smallImage.src = imageData;
	jQuery("#post-update-content").val('');
	jQuery("#post-update-messages").html('<h3 style="color:red;">Posted!</h3>');
	jQuery("#post-update-messages").slideDown("fast");
	jQuery("#post-update-messages").delay(2000).slideUp("fast");
}

function fail(error) {
    alert("An error has occurred: Code = " + error.code);
    cl("upload error source " + error.source);
    cl("upload error target " + error.target);
	jQuery("#post-update-messages").html('<h3 style="color:red;">Whoa! Something did not work! Try again...</h3>');
	jQuery("#post-update-messages").slideDown("fast");
	jQuery("#post-update-messages").delay(2000).slideUp("fast");
	jQuery.mobile.loading('hide');
}

}// end else ~126
});// end on change

function uint8ToString(buf) {
    var i, length, out = '';
    for (i = 0, length = buf.length; i < length; i += 1) {
        out += String.fromCharCode(buf[i]);
		console.log("output of uint8ToString function: ")
		console.log(out)
    }
    return out;
}

}// MainUploaderFunction
function readImageData(input){
	console.log("readImageData")
	bannerImage = document.getElementById('file-to-upload');
	
	//imageData = btoa(bannerImage);
	//localStorage.setItem("satImageObject", imageData);
	var imgB64 = getBase64Image(bannerImage);
	console.log(imgB64);
	
}//readImageData

function getBase64Image(img) {
    // Create an empty canvas element
	//var image = new Image();
	//image.src = img;
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to guess the
    // original format, but be aware the using "image/jpg" will re-encode the image.
    var dataURL = canvas.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function getImageDataURL(url, success, error) {
	var data, canvas, ctx;
	var img = new Image();
	img.onload = function(){
		// Create the canvas element.
	    canvas = document.createElement('canvas');
	    canvas.width = img.width;
	    canvas.height = img.height;
		// Get '2d' context and draw the image.
		ctx = canvas.getContext("2d");
	    ctx.drawImage(img, 0, 0);
		// Get canvas data URL
		try{
			data = canvas.toDataURL();
			success({image:img, data:data});
		}catch(e){
			error(e);
		}
	}
	// Load image URL.
	try{
		img.src = url;		
	}catch(e){
		error(e);
	}
}// getImageDataURL

function gotPic(event) {
        if(event.target.files.length == 1 && 
           event.target.files[0].type.indexOf("image/") == 0) {
            	
				var dataURL = jQuery("#file-to-upload").attr("src",URL.createObjectURL(event.target.files[0]));
				var theData = URL.createObjectURL(event.target.files[0]);
				theData = theData.replace('blob:','')
				console.log(theData);
				var files = event.target.files,
				file;
			if (files && files.length > 0) {
				file = files[0];
			}	
				var imgURL = URL.createObjectURL(file);
				console.log(imgURL);
				//getBase64Image(imgURL);
				var reader = new window.FileReader();
 				reader.readAsDataURL(imgURL); 
 					var base64data = reader.result;                
                	console.log(base64data );
  					
        }
	}
	
function getPics(event){

var files = event.target.files, file;
	if (files && files.length > 0) {
		file = files[0];
		var fileReader = new FileReader();
		fileReader.onload = function (event) {
			base64Image = event.target.result;
			imageFile = base64Image.replace('data:image/jpeg;base64,','');
			window.localStorage.setItem("satImageObject",imageFile);
		};
		fileReader.readAsDataURL(file);
	}
	
	}// getPics