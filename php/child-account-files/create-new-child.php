<?php
if (!defined("SATLOC")) {
	$SATLOC = $_POST['SATLOC'];
	define("SATLOC",$SATLOC);
	}

$location = $_SERVER['DOCUMENT_ROOT']. SATLOC;
include ($location . '/wp-config.php');
include ($location . '/wp-load.php');
include ($location . '/wp-includes/pluggable.php');
global $wpdb;


$fname 			=	mysql_escape_string($_POST['first_name']);
$lname 			=	mysql_escape_string($_POST['last_name']);
$email 			=	mysql_escape_string($_POST['user_email']);
$user_name		=	mysql_escape_string($_POST['user_name']);
$zip			=	mysql_escape_string($_POST['zip']);
$year			=	mysql_escape_string($_POST['year']);
$month			=	mysql_escape_string($_POST['month']);
$pass			=	mysql_escape_string($_POST['pass']);
$hash_pass		=	wp_hash_password($pass);
$parent_id		=	mysql_escape_string($_POST['parent_id']);
$st_user_data	=	array 
				(
					user_pass				=>	$pass, 
					user_login				=>	$user_name, 
					user_nicename			=>	$user_name, 
					user_email				=>	$email, 
					display_name			=>	$fname . ' ' . $lname, 
					first_name				=>	$fname, 
					last_name	 			=>	$lname, 
					show_admin_bar_front	=>	'false'
				);

get_header();
echo '<div id="new-user-form">';
echo '<!--';
echo '<p>	First Name: ' .	$fname 	. '</p>';
echo '<p>	Last Name: ' .	$lname 	. '</p>';
echo '<p>	Email Addy: ' .	$email 	. '</p>';
echo '<p>	User Name: ' .	$user_name	. '</p>';
echo '<p>	Password: ' .	$pass	. '</p>';
echo '<p>	Hash Pass: ' .	$hash_pass	. '</p>';
echo '<p>	Parent ID: ' .	$parent_id	. '</p>';
echo '<p>	Child Age: ' .	$age	. '</p>';
echo '<p>	Child Zip: ' .	$zip	. '</p>';
echo '<p>	Year: ' .	$year	. '</p>';
echo '-->';


$new_user 	=	wp_insert_user( $st_user_data );

$insert_year = $wpdb->get_results("
INSERT INTO  `wp_bp_xprofile_data` (
`id` ,
`field_id` ,
`user_id` ,
`value` ,
`last_updated`
)
VALUES (
NULL , 
'59', 
'$user_name', 
'$year', 
NOW()
);
");

$insert_month = $wpdb->get_results("
INSERT INTO  `wp_bp_xprofile_data` (
`id` ,
`field_id` ,
`user_id` ,
`value` ,
`last_updated`
)
VALUES (
NULL ,  '46',  '$user_name',  '$month',  NOW()
);
");

$insert_zip = $wpdb->get_results("
INSERT INTO  `wp_bp_xprofile_data` (
`id` ,
`field_id` ,
`user_id` ,
`value` ,
`last_updated`
)
VALUES (
NULL ,  '36',  '$user_name',  '$zip',  NOW()
);
");

add_user_meta( $new_user, 'st_parent_id', $parent_id);
add_user_meta( $new_user, 'st_active_child', 1);


if (is_numeric($new_user)) {
global $bp;	
$new_user_name = get_userdata( $new_user );
echo '<h3>You have successfully created an account for ' . $new_user_name->display_name . '.</h3>';
echo '<h3><a href="' . $bp->loggedin_user->domain . '">Go back to your account.</a></h3>';
}
else {
	echo '<h3>' . $new_user->get_error_message() . '</h3>';
	echo '<p>Hit the back button on your browser and make a new user name for your child</p>';
	}

echo '</div>';

get_sidebar();
get_footer();

?>