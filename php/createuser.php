<?php

class JSON_API_createUser_Controller {

// look for the $tmp_server = 'JustynMachineCode';


/********************************************/
/********  CHECK IF USER/EMAIL EXIST ********/
/********************************************/

public function checkUsernameEmail_api() {
	global $json_api;
	global $wpdb;
	
$newUsername 	= $_GET['user'];
$newEmail 		= $_GET['eml'];

if(!$newUsername){
	return array("status" => "No Username provided!");
	}
if(!$newEmail){
	return array("status" => "No Email provided!");
	}


$usernameCheck = $wpdb->get_results(
"SELECT ID
FROM  `wp_users` 
WHERE user_nicename =  '$newUsername'
OR user_login =  '$newUsername'
LIMIT 1
");

$userFound = $wpdb->num_rows;

$emailCheck = $wpdb->get_results(
"SELECT ID
FROM  `wp_users` 
WHERE user_email =  '$newEmail'
");

$emailFound = $wpdb->num_rows;

// Build messages
if($userFound == 1){$message = "That Username is already in use. Try another!";$status="error";}
if($emailFound >= 7){$message = "That email has been used too many times!";$status="error";}
if($emailFound >= 7 && $userFound ==1){$message = "That Username is already in use and that email has been used too many times!";$status="error";}
if($emailFound <= 6 && $userFound ==0) {$message = "Username and email are available! Carry on...";$status="ok";}

return array(
	"status" 	=> $status,
	"user"		=> $userFound,
	"email"		=> $emailFound,
	"message"	=> $message
);

}// end 


/********************************************/
/*********  CREATE A MAIN ACCOUNT ***********/
/********************************************/

public function createUser_api() {
		global $json_api;
		global $wpdb;
//
//	types:
//	0 = not activated
//	1 = activated via SMS
//	2 = not activated without SMS
//	3 = activated without SMS
//	4 = child account

// SET THESE FIELDS BASED UPON SERVER
// LOCAL MACHINE
// child account 	= 27
// postal code 		= 34
// year 			= 15
// country 			= 30
// phone 			= 35

// DEV SERVER
// child account 	= 27
// postal code 		= 34
// year 			= 15
// country 			= 30
// phone 			= 35

// BETA SERVER
// child account 	= 27
// postal code 		= 34
// year 			= 15
// country 			= 30
// phone 			= 35

// PRODUCTION SERVER
// child account 	= 27
// postal code 		= 36
// year 			= 59
// country 			= 42
// phone 			= 121
// phone country	= 122


$year_field_id				= 59;
$parent_account_field_id 	= 4;
$country_field_id			= 42;
$postal_code_field_id		= 36;
$phone_field_id				= 121;
$phone_country_id			= 122;
						 
		$user 		= mysql_escape_string($_GET['username']);
		$pass		= mysql_escape_string($_GET['pass']);
		$email		= mysql_escape_string($_GET['email']);
		$tos		= mysql_escape_string($_GET['tos']);
		$parent		= mysql_escape_string($_GET['parent']);	
		$year		= mysql_escape_string($_GET['year']);
		$postal		= mysql_escape_string($_GET['postal']);	
		$country	= mysql_escape_string($_GET['country']);
		$phone		= mysql_escape_string($_GET['phone']);
		$type 		= intval(mysql_escape_string($_GET['user_activation_type']));
		$phoneCountry	= mysql_escape_string($_GET['phonecountry']);
		$parentAccount	= mysql_escape_string($_GET['parent']);
		$eno	= 0;
		
			
		/** Validate email address **/
		if (strpos($email,'@') == false) {
		    $eno++;
			$email_invalid = '<p style="color:red;">• That email doesn\'t look right.</p>';
		}

		
		if($tos != 'true') {
			$message_tos = '<p style="color:red;">• You must agree to the Terms of Service.</p>';
			$eno++;
		}
		if(!$user) {
			$message_user = '<p style="color:red;">• You cannot leave the Username field blank.</p>';
			$eno++;
		}
		if(!$pass) {
			$message_pass = '<p style="color:red;">• You have to create a password.</p>';
			$eno++;
		}
		if(!$email) {
			$message_email = '<p style="color:red;">• You have to provide an email address.</p>';
			$eno++;
		}
		if(!$year) {
			$message_year = '<p style="color:red;">• You have to provide a year of birth.</p>';
			$eno++;
		}
		if(!$postal) {
			$message_postal = '<p style="color:red;">• You have to provide a postal code.</p>';
			$eno++;
		}
		if(!$country) {
			$message_country = '<p style="color:red;">• You must select a country.</p>';
			$eno++;
		}
		
		/** Return error message if inputs are empty **/
		if($eno>0) {$error_message = $email_invalid . $message_tos . $message_pass . $message_email . $message_user . $message_year . $message_postal . $message_country;
		return array(
			"status" 	=> "error",
			"message"	=> $error_message
	    );			
		} // end if		
		
		/** If no blank inputs, attempt to create a new user **/

		if($eno==0) {

		$new_user_id = wp_create_user( $user, $pass, $email );	
			if(is_object($new_user_id)) {
				return array(
					"status" 	=> "error",
					"message"	=> $new_user_id
				);
				}
/********* Create new user **********/
			elseif($new_user_id>0) {				
		
			// POSTAL CODE INSERT
			$zip = $postal;
			$zip_insert = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id`,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL ,  '$postal_code_field_id',  '$new_user_id',  '$zip',  ''
					);
				");						
			
			// COUNTRY INSERT
			$country_insert = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id`,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL ,  '$country_field_id',  '$new_user_id',  '$country',  ''
					);
				");			
			
			// YEAR INSERT
			$year_insert = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id`,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL ,  '$year_field_id',  '$new_user_id',  '$year',  ''
					);
				");		
			
			// FIND DEFAULT TALENT
			$default_talent_query = $wpdb->get_results("
			SELECT id 
			FROM  `sat_talents` 
			WHERE top_level =  'other'
			AND level_2 =  'other'
			AND level_3 =  'other'			
			");
			$default_talent = 124;
			// DEFAULT TALENT
			$talent_insert = $wpdb->get_results("
					INSERT INTO `sat_01_talent` (
						`id` ,
						`user_id` ,
						`talent_id` 						
						)
						VALUES (
						NULL ,  '$new_user_id',  '124'
						);
				");	
			
			// PARENT INSERT			
			$parent_insert = $wpdb->get_results("
			INSERT INTO `wp_bp_xprofile_data` (
			`id`, 
			`field_id`, 
			`user_id`, 
			`value`, 
			`last_updated`
			) 
			VALUES (
				NULL, 
				'$parent_account_field_id', 
				'$new_user_id', 
				'$parentAccount', 
				NOW()
				)
			");
			
			// COUNTRY INSERT			
			$country_insert = $wpdb->get_results("
			INSERT INTO `wp_bp_xprofile_data` (
			`id`, 
			`field_id`, 
			`user_id`, 
			`value`, 
			`last_updated`
			) 
			VALUES (
				NULL, 
				'$country_field_id', 
				'$new_user_id', 
				'$country', 
				NOW()
				)
			");
			
			if($type == 2){$phone = '0';}
			// PHONE INSERT			
			$phone_insert = $wpdb->get_results("
			INSERT INTO `wp_bp_xprofile_data` (
			`id`, 
			`field_id`, 
			`user_id`, 
			`value`, 
			`last_updated`
			) 
			VALUES (
				NULL, 
				'$phone_field_id', 
				'$new_user_id', 
				'$phone', 
				NOW()
				)
			");
			
			// PHONE COUNTRY INSERT			
			$phone_country_insert = $wpdb->get_results("
			INSERT INTO `wp_bp_xprofile_data` (
			`id`, 
			`field_id`, 
			`user_id`, 
			`value`, 
			`last_updated`
			) 
			VALUES (
				NULL, 
				'$phone_country_id', 
				'$new_user_id', 
				'$phoneCountry', 
				NOW()
				)
			");
			
			

/*************/
			
			// ACTIVATION CODE
			$t = microtime(true);
			$micro = sprintf("%06d",($t - floor($t)) * 1000000);
			$d = new DateTime( date('Y-m-d H:i:s.'.$micro,$t) );
			$date = $d->format("Y-m-d H:i:s.u");
			$hash = mb_strtoupper(hash('crc32b', $date));
			
			$hash_insert = $wpdb->get_results("
					INSERT INTO  `sat_hash` (
						`id` ,
						`user_id`,
						`hash`,
						`status` 						
						)
						VALUES (
						NULL ,  '$new_user_id',  '$hash', '$type'
						);
				");							
			
			// if by phone
			if($type == 0){
				$phone_source = $phoneCountry;				
				$smsURL = 'https://api.clickatell.com/http/sendmsg?user=ShareATalent&password=Acgtech2013&api_id=3458862&mo=1&from=19726967021&to='.$phone_source.$phone.'&text=Your%20ShareATalent%20Activation%20Code%20is:%20'.$hash;
			$sendSMS = file_get_contents($smsURL);
			$store_response = $wpdb->get_results("
				INSERT INTO  `sha1306407571361`.`sat_clickatell_tracker` (
					`id` ,
					`user_id` ,
					`clickatell_response` ,
					`smsURL` ,
					`time`
					)
					VALUES (
					NULL ,  '$new_user_id',  '$sendSMS', '$smsURL', NOW()
					);
			");
			return array(
					"status" 	=> "ok",					
					"user_id"	=> $new_user_id,
					"hash"		=> $hash,
					"username"	=> $user,
					"email"		=> $email,
					"type"		=> $type,
					"debug"		=> $smsURL,
					"debug2"	=> $sendSMS
				);
				exit;
			} elseif($type == 2){
			
			$rand1 	= rand(1,9);
			$rand2 	= rand(1,9);
			$answer	= $rand2 + $rand1;
			$security_question = 'What is '.$rand1.' plus '.$rand2.'?';
			
			$security_insert = $wpdb->get_results("
					INSERT INTO  `sat_verification_equation` (
						`id` ,
						`user_id`,
						`equation`,
						`answer` 						
						)
						VALUES (
						NULL ,  '$new_user_id',  '$security_question', '$answer'
						);
				");	
				
			$user_info = get_userdata($new_user_id);
			$to = $user_info->user_email;           
			$un = $user_info->user_login;           
			
			$message = '<p>Hey there, ' . $un . '!</p>';
			$message .= "<br />";
			$message .= '<p>Welcome to Share a Talent! Your login information is below. To activate your account, login and paste the activation code below into the form. </p>';
			$message .= '<p>Username: '.$un.'</p>';
			$message .= '<p>Password: (what you entered)</p>';
			$message .= '<p>Activation Code: '.$hash.'</p>';
			if($type==2){$message .= '<p>Security Question: '.$security_question.'</p>';}
			$message .= '<p>Login on your Share A Talent app or';
			$message .= ' at <a href="http://www.shareatalent.com">http://www.shareatalent.com</a></p>';

			////////////////////
error_reporting(E_ALL);			

$mailLocation2 = (dirname(__FILE__).'.\..\..\..\themes\bp-talent-sharing\mail\class.phpmailer.php');	
$mailLocation = $_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/bp-talent-sharing/mail/class.phpmailer.php';	


require_once($mailLocation);
	
try {
			$phpmailer = new PHPMailer(true);	
			$phpmailer->IsSMTP();
			//$phpmailer->SMTPDebug = 2;
			$phpmailer->SMTPAuth = true;
			$phpmailer->Host = 'smtpout.secureserver.net';
			$phpmailer->Port = 80;
			$phpmailer->Username = 'info@shareatalent.com';
			$phpmailer->Password = 'Acgtech2013';
			$phpmailer->AddAddress($email);
			$phpmailer->SetFrom('info@shareatalent.com', 'Share A Talent');
			$phpmailer->AddReplyTo('info@shareatalent.com', 'Share A Talent');
			$phpmailer->Subject = 'Share A Talent - Member Verification';
			$phpmailer->Body = $message;
			$phpmailer->IsHTML(true);
			$phpmailer->IsSMTP();
			$phpmailer->Send();
			//$debug .= "Message Sent OK<p></p>\n";
			} catch (phpmailerException $e) {
				echo 'error1';
				echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo 'error2';
				echo $e->getMessage(); //Boring error messages from anything else!
			}				
			////////////////////

			$debug = "no phone";
			}// end if phone = 0
			
			elseif ($phone >= 1){
				$debug = "phone";
				
				}// end if phone >=1
			// RETURN ARRAY
			return array(
					"status" 	=> "ok",
					"user_id"	=> $new_user_id,
					"hash"		=> $hash,
					"username"	=> $user,
					"email"		=> $email,
					"type"		=> $type					
				);
			} // end if successfull new user created
			else {
				return array(
					"status" 		=> "error",
					"error_message"	=> $new_user_id
				);
			} // end else if account is created 
		} // end if	new account is NOT created

		
  }// end function
  
/********************************************/
/*************  VERIFY ACCOUNT **************/
/********************************************/

public function verifyAccount() {
		global $json_api;
		global $wpdb;

	$activation_code	= mysql_escape_string($_GET['act']);	
	$security_answer	= mysql_escape_string($_GET['sec']);	
	$user_id			= mysql_escape_string($_GET['uid']);
	
$answer_query = $wpdb->get_results("
	SELECT *
	FROM  `sat_verification_equation` 
	WHERE user_id = $user_id
");

$attempts = $answer_query[0]->attempts;
if($attempts >= 3){
	return array(
	'status' 	=> 'error',
	'message'	=> 'Too many failed attempts to activate your account! Contact support@shareatalent.com'
	);
	}

$hash_query = $wpdb->get_results("
	SELECT hash
	FROM  `sat_hash` 
	WHERE user_id = $user_id
");

if($answer_query[0]->answer == $security_answer){$sec_res = true;} else {$sec_res = false;}
if($hash_query[0]->hash == $activation_code){$act_res = true;} else {$act_res = false;}

if($sec_res == true && $act_res == true) {
	$activate_account = $wpdb->update( 
	'sat_hash', 
	array( 
		'status' => 3,	// string
	), 
	array( 'user_id' => $user_id )
	);
	
	return array(
	"status" => "ok",
	"activation_code"	=> $activation_code,
	"security_answer"	=> $security_answer,
	"user_id"			=> $user_id,
	"activation_result" => $act_res,
	"security_result" 	=> $sec_res,
	"activated"			=> $activate_account
);	
	
}// end if all are true
else {
	$answers_left = 3 - $attempts;
	if($answers_left == 1 ){$try_or_tries = 'try';} else {$try_or_tries = 'tries';}
	$attempts++;
	$update_attempts = $wpdb->get_results("
	UPDATE  `sat_verification_equation` SET  `attempts` =  '$attempts' WHERE  `user_id` =$user_id;
	");
	
	return array(
	"status"	=> "error",
	"message"	=> "Either your activation code or your security answer were incorrect. You have ".$answers_left." more ".$try_or_tries."."
	);
	}
	

		
}// end verifyAccount
 
/***********************************************/
/*********  RESEND VERIFICATION EMAIL **********/
/***********************************************/
public function resendVerification() {
		global $json_api;
		global $wpdb;
		
$user_id = $_GET['user'];

$user_info = get_userdata($user_id);
$email = $user_info->user_email;           

$un = $user_info->user_login; 


$hash_query = $wpdb->get_results("
	SELECT * from `sat_hash` 
	WHERE user_id = $user_id;
");
$hash = $hash_query[0]->hash;

$security_query = $wpdb->get_results("
	SELECT * from `sat_verification_equation` 
	WHERE user_id = $user_id;
");

$hash = $hash_query[0]->hash;
$security_question = $security_query[0]->equation;

	$message = '<p>Hey there, ' . $un . '!</p>';
	$message .= "<br />";
	$message .= '<p>Looks like you asked us to send you the verification info again. No problem. We\'re good at that stuff! </p>';
	$message .= '<p>Username: '.$un.'</p>';
	$message .= '<p>Activation Code: '.$hash.'</p>';
	$message .= '<p>Security Question: '.$security_question.'</p>';
	$message .= '<p>See what I\'m saying. We got this!</p>';

			////////////////////
error_reporting(E_ALL);			

$mailLocation2 = (dirname(__FILE__).'\..\..\..\themes\bp-talent-sharing\mail\class.phpmailer.php');	
$mailLocation = $_SERVER["DOCUMENT_ROOT"].'/wp-content/themes/bp-talent-sharing/mail/class.phpmailer.php';	


require_once($mailLocation2);
	
try {
			$phpmailer = new PHPMailer(true);	
			$phpmailer->IsSMTP();
			//$phpmailer->SMTPDebug = 2;
			$phpmailer->SMTPAuth = true;
			$phpmailer->Host = 'smtpout.secureserver.net';
			$phpmailer->Port = 80;
			$phpmailer->Username = 'info@shareatalent.com';
			$phpmailer->Password = 'Acgtech2013';
			$phpmailer->AddAddress($email);
			$phpmailer->SetFrom('info@shareatalent.com', 'Share A Talent');
			$phpmailer->AddReplyTo('info@shareatalent.com', 'Share A Talent');
			$phpmailer->Subject = 'Share A Talent - Member Verification';
			$phpmailer->Body = $message;
			$phpmailer->IsHTML(true);
			$phpmailer->IsSMTP();
			$phpmailer->Send();

			} catch (phpmailerException $e) {
				echo 'error1';
				echo $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
				echo 'error2';
				echo $e->getMessage(); //Boring error messages from anything else!
			}	

return array(
	"status" 	=> "ok",
	"message" 	=> "Email sent to ".$email,
	"hash"		=> $hash,
	"security"	=> $security_question,
	"email"		=> $email
);
		
}// end resend verification


/********************************************/
/*********  CREATE A CHILD ACCOUNT **********/
/********************************************/
public function createChild_api() {
		global $json_api;
		global $wpdb;
		
		 
		$user 	= mysql_escape_string($_GET['username']);
		$pass	= mysql_escape_string($_GET['pass']);
		$email	= mysql_escape_string($_GET['email']);
		$first	= mysql_escape_string($_GET['first']);
		$last	= mysql_escape_string($_GET['last']);
		$zip	= mysql_escape_string($_GET['zip']);
		$year	= mysql_escape_string($_GET['year']);
		$month	= mysql_escape_string($_GET['month']);
		$parent	= mysql_escape_string($_GET['parent']);		

		$eno = 0;

		// Validate email address
		if ($email) {
		if (strpos($email,'@') == false) {
		    $eno++;
			$email_invalid = '<p style="color:red;">• That email doesn\'t look right.</p>';
		}}
		
		if(!$user) {
			$message_user = '<p style="color:red;">• You cannot leave the Username field blank.</p>';
			$eno++;
		}
		if(!$pass) {
			$message_pass = '<p style="color:red;">• You have to create a password.</p>';
			$eno++;
		}
		if(!$email) {
			$message_email = '<p style="color:red;">• You have to provide an email address.</p>';
			$eno++;
		}
		if(!$month) {
			$message_month = '<p style="color:red;">• You have to provide a birth month.</p>';
			$eno++;
		}
		if(!$year) {
			$message_year = '<p style="color:red;">• You have to provide a birth month.</p>';
			$eno++;
		}
		if(!$first) {
			$message_first = '<p style="color:red;">• You have to provide a first name.</p>';
			$eno++;
		}
		if(!$last) {
			$message_last = '<p style="color:red;">• You have to provide a last name.</p>';
			$eno++;
		}
		if(!$zip) {
			$message_zip = '<p style="color:red;">• You have to provide a postal/zip code.</p>';
			$eno++;
		}
		
		// Return error message if inputs are empty 
		if($eno>0) {$error_message = $email_invalid . $message_user . $message_pass . $message_email . $message_month . $message_year . $message_first . $message_last . $message_zip;
		return array(
			"status" 	=> "error",
			"message"	=> $error_message
	    );			
		} // end if		

		// If no blank inputs, attempt to create a new user 
		if($eno==0) {
		$new_user_id = wp_create_user( $user, $pass, $email );	
			if(is_object($new_user_id)) {
				return array(
					"status" 	=> "error",
					"message"	=> $new_user_id
				);
				}
			// Successful child created, now add meta content
			elseif($new_user_id>0) {				
				add_user_meta( $new_user_id, 'st_parent_id', $parent);		
				add_user_meta( $new_user_id, 'st_active_child', 1);
				// Add Year
				$insert_year = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id` ,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL , 
						'59', 
						'$new_user_id', 
						'$year', 
						NOW()
					);
					");
					
				$insert_month = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id` ,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL ,  '46',  '$new_user_id',  '$month',  NOW()
					);
					");
					
				$insert_zip = $wpdb->get_results("
					INSERT INTO  `wp_bp_xprofile_data` (
						`id` ,
						`field_id` ,
						`user_id` ,
						`value` ,
						`last_updated`
					)
					VALUES (
						NULL ,  '36',  '$new_user_id',  '$zip',  NOW()
					);
					");
				return array(
					"status"	=> "ok",
					"message"	=> "Child Account Created!"
				);
			} // end if successfull new user created
			else {
				return array(
					"status" 		=> "error",
					"message"	=> $new_user_id
				);
				} // end else
		} // end if	

		
  }// end function  



}// end class

?>