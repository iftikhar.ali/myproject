<?php 
$location = $_SERVER['DOCUMENT_ROOT'];
require_once($location . '/wp-load.php'); 
require_once($location . '/wp-config.php'); 
global $wpdb; 
global $bp;

$userID = $_GET['user'];

$activities = $wpdb->get_results("
	SELECT id, content 
	FROM  `wp_bp_activity` 
	WHERE user_id = $userID
	AND component = 'activity'
");

if ( $activities ){
	echo json_encode(array(
		'status'		=> 'ok',
		'activities'	=> $activities
	));
} else {
	echo json_encode(array(
		'status'	=> 'error',
		'message'	=> 'No activities posted yet!'
	));	
} 