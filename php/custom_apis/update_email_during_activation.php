<?php
include_once('api_location.php');

$location = api_location();
require_once($location . '/wp-load.php'); 
require_once($location . '/wp-config.php'); 
global $wpdb; 
global $bp;

$user_id = mysql_escape_string($_GET['user_id']);
$old_email = mysql_escape_string($_GET['old']);
$new_email	= mysql_escape_string($_GET['new']);

$update = $wpdb->update(
	"wp_users",
	array("user_email" => $new_email),
	array(
		"ID" => $user_id, 
		"user_email" => $old_email
		)
	);

if($update == 1){
	echo json_encode(array(
		"status" => "ok",
		"update" => $update
	));
	}
else {
	echo json_encode(array(
		"status" 	=> "error",
		"update" 	=> $update,
		"error"		=> stripslashes($wpdb->last_query),
		"location"	=> $location
	));
}


?>