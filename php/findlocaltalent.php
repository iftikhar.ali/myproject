<?php

/*
Controller Name: Find Local Talent
Controller Description: Share A Talent - Find Local Talent functionality
Controller Author: Justyn Hornor
Controller Author Twitter: @jphornor
*/


class JSON_API_findlocaltalent_Controller {

public function localTalent_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= mysql_escape_string($_GET['userid']);
$search 	= mysql_escape_string($_GET['search']);
$talent_id 	= mysql_escape_string($_GET['talentid']);
$r=0;

if($search == NULL && $talent_id == NULL) {

// User Country
$user_country_query = $wpdb->get_results("
SELECT value
FROM `wp_bp_xprofile_data`
WHERE user_id = $userid
AND field_id = 42
LIMIT 1
");

$country = $user_country_query[0]->value;
if ($country == NULL) {$country = 'United States';}

// User Zip
$user_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $userid
AND field_id = 36
LIMIT 1
");

$zip = $user_zip[0]->value;

// Long/Lat of User 
if($country == 'United States'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes`
WHERE postal_code =  $zip
LIMIT 1
");
}

if($country == 'India'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_india`
WHERE postal_code =  $zip
LIMIT 1
");
}
if($country=='Germany'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany`
WHERE postal_code =  $zip
LIMIT 1
");
}// end if

$ulong = $user_long_lat[0]->longitude;
$ulat = $user_long_lat[0]->latitude;

// User Talent ID
$user_primary_talent = $wpdb->get_results("
SELECT *
FROM `sat_01_talent`
WHERE user_id = $userid
LIMIT 1
");

$debug 		= $wpdb->last_query;
$talentid 	= $user_primary_talent[0]->talent_id;

// User Talent Name
$primary_talent_name = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $talentid
LIMIT 1
");

$talent_top 	= $primary_talent_name[0]->top_level;
$talent_sec 	= $primary_talent_name[0]->level_2;
$talent_name 	= $primary_talent_name[0]->level_3;

// Lookup users with same talentid
$search_the_keyword = $wpdb->get_results("
SELECT user_id
FROM  `sat_01_talent` 
WHERE talent_id = $talentid
");

// Lookup location of matching users

$map = array();
$results = array();
$i=0;
foreach ($search_the_keyword as $sk) {

// lookup other group's zip codes
$g_id	=	$sk->user_id;
$other_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 36
LIMIT 1
");
$other_country	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 42
LIMIT 1
");

$o_name = $wpdb->get_results("
SELECT *
FROM `wp_users`
WHERE ID = $g_id
LIMIT 1
");

$g_nam = $o_name[0]->user_nicename;

$oz	=	$other_zip[0]->value;
$oc = 	$other_country[0]->value; // country lookup

// Get long/lat of the group's zip code
if ($oc == 'United States') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'India') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_india` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'Germany') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany` 
WHERE postal_code = $oz
");
}

$u_city 	= $zlookup[0]->place_name;
$u_state 	= $zlookup[0]->place_name2;


// Assign current user and group's long/lat
$point1['lat'] 		= $user_long_lat[0]->latitude;
$point1['long'] 	= $user_long_lat[0]->longitude;
$point2['lat'] 		= $zlookup[0]->latitude;
$point2['long'] 	= $zlookup[0]->longitude;
$point2['city'] 	= $zlookup[0]->place_name;
$point2['state'] 	= $zlookup[0]->place_name2;

	// Calculate the distance from current user to group's long/lat
    $distance = (3958 * 3.1415926 * sqrt(
    		($point1['lat'] - $point2['lat'])
    		* ($point1['lat'] - $point2['lat'])
    		+ cos($point1['lat'] / 57.29578)
    		* cos($point2['lat'] / 57.29578)
    		* ($point1['long'] - $point2['long'])
    		* ($point1['long'] - $point2['long'])
    	) / 180);

	// If the distance is under 55 miles, include the group
	if ($distance < 55 ) {
		$map[$i] = array(
			'o_id' 		=> $g_id,
			'o_long' 	=> $point2['long'],
			'o_lat' 	=> $point2['lat'],
			'o_city'	=> $point2['city'],
			'o_state'	=> $point2['state'],
			'o_zip'		=> $oz
		); 	
		$mob_map[$i]	= array (
			'user_id' 		=> $g_id,
			'user_name'		=> $g_nam,
			'user_city'		=> $u_city,
			'user_state'	=> $u_state
		);

		
		// Build arrays for map and results
		$map_group[$i] = intval($oz);		
		$results[$i] = $g_id;
		$i++;
	}// end if 

} // end foreach



return array(
"status" 	=> "ok",
"userid" 	=> $userid,
"zip"		=> $zip,
"country"	=> $country,
"ulong"		=> $ulong,
"ulat"		=> $ulat,
"talentid"	=> $talentid,
"talentTop"	=> $talent_top,
"talentSec"	=> $talent_sec,
"talentName"=> $talent_name,
"results"	=> $mob_map,
"debug1"		=> $debug
);
		
  
}// end if search is blank

/**************************************************************/
/**************************************************************/
/**************************************************************/
/**************************************************************/
/*						Search for a Talent 				  */
/**************************************************************/
/**************************************************************/
/**************************************************************/
/**************************************************************/

else {

// User Country
$user_country_query = $wpdb->get_results("
SELECT value
FROM `wp_bp_xprofile_data`
WHERE user_id = $userid
AND field_id = 42
LIMIT 1
");

$country = $user_country_query[0]->value;
if ($country == NULL) {$country = 'United States';}

// User Zip
$user_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $userid
AND field_id = 36
LIMIT 1
");

$zip = $user_zip[0]->value;

// Long/Lat of User 
if($country == 'United States'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes`
WHERE postal_code =  $zip
LIMIT 1
");
}

if($country == 'India'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_india`
WHERE postal_code =  $zip
LIMIT 1
");
}
if($country=='Germany'){
$user_long_lat = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany`
WHERE postal_code =  $zip
LIMIT 1
");
}// end if

$ulong = $user_long_lat[0]->longitude;
$ulat = $user_long_lat[0]->latitude;

// Did user select a recommended search?
if ($talent_id != NULL) {	
	$talentid = $talent_id;
// User Talent Name
$recommended_search_talent = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE id = $talentid
LIMIT 1
");

$talent_top 	= $recommended_search_talent[0]->top_level;
$talent_sec 	= $recommended_search_talent[0]->level_2;
$talent_name 	= $recommended_search_talent[0]->level_3;

}
else if(!$talent_id){

// Search term
$search_word = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE level_3 LIKE '%$search%'
LIMIT 1
");

$talentid 		= $search_word[0]->id;
$debug = $search_word[0]->id;

// Recommended Search Terms
$recommended_search_terms = $wpdb->get_results("
SELECT *
FROM `sat_talents`
WHERE level_3 LIKE '%$search%'
");

$talent_top 	= $search_word[0]->top_level;
$talent_sec 	= $search_word[0]->level_2;
$talent_name 	= $search_word[0]->level_3;
}

// Lookup users with same talentid
$search_the_keyword = $wpdb->get_results("
SELECT user_id
FROM  `sat_01_talent` 
WHERE talent_id = $talentid
");

// Lookup location of matching users

$map = array();
$results = array();
$i=0;
foreach ($search_the_keyword as $sk) {

// lookup other group's zip codes
$g_id	=	$sk->user_id;
$other_zip	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 36
LIMIT 1
");
$other_country	= $wpdb->get_results("
SELECT *
FROM `wp_bp_xprofile_data`
WHERE user_id = $g_id
and field_id = 42
LIMIT 1
");

$o_name = $wpdb->get_results("
SELECT *
FROM `wp_users`
WHERE ID = $g_id
LIMIT 1
");

$g_nam = $o_name[0]->user_nicename;

$oz	=	$other_zip[0]->value;
$oc = 	$other_country[0]->value; // country lookup

// Get long/lat of the group's zip code
if ($oc == 'United States') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'India') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_india` 
WHERE postal_code = $oz
");
}

elseif ($oc == 'Germany') {
$zlookup = $wpdb->get_results("
SELECT *
FROM `zipcodes_germany` 
WHERE postal_code = $oz
");
}

$u_city 	= $zlookup[0]->place_name;
$u_state 	= $zlookup[0]->place_name2;


// Assign current user and group's long/lat
$point1['lat'] 		= $user_long_lat[0]->latitude;
$point1['long'] 	= $user_long_lat[0]->longitude;
$point2['lat'] 		= $zlookup[0]->latitude;
$point2['long'] 	= $zlookup[0]->longitude;
$point2['city'] 	= $zlookup[0]->place_name;
$point2['state'] 	= $zlookup[0]->place_name2;

	// Calculate the distance from current user to group's long/lat
    $distance = (3958 * 3.1415926 * sqrt(
    		($point1['lat'] - $point2['lat'])
    		* ($point1['lat'] - $point2['lat'])
    		+ cos($point1['lat'] / 57.29578)
    		* cos($point2['lat'] / 57.29578)
    		* ($point1['long'] - $point2['long'])
    		* ($point1['long'] - $point2['long'])
    	) / 180);

	// If the distance is under 55 miles, include the group
	if ($distance < 55 ) {
		$map[$i] = array(
			'o_id' 		=> $g_id,
			'o_long' 	=> $point2['long'],
			'o_lat' 	=> $point2['lat'],
			'o_city'	=> $point2['city'],
			'o_state'	=> $point2['state'],
			'o_zip'		=> $oz
		); 	
		$mob_map[$i]	= array (
			'user_id' 		=> $g_id,
			'user_name'		=> $g_nam,
			'user_city'		=> $u_city,
			'user_state'	=> $u_state
		);

		
		// Build arrays for map and results
		$map_group[$i] = intval($oz);		
		$results[$i] = $g_id;
		$i++;
	}// end if 

} // end foreach



return array(

"status" 	=> "ok",
"userid" 	=> $userid,
"zip"		=> $zip,
"country"	=> $country,
"ulong"		=> $ulong,
"ulat"		=> $ulat,
"talentid"	=> $talentid,
"talentTop"	=> $talent_top,
"talentSec"	=> $talent_sec,
"talentName"=> $talent_name,
"search"	=> $search_word,
"recommend"	=> $recommended_search_terms,
"results"	=> $mob_map,
"debug2"	=> $debug
);
	
	} // end else if search not blank
	
  }// end search blank function
}// end class
?>