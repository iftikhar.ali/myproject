<?php 
if (!defined("SATLOC")) {
	$SATLOC = '/' . $_POST['satloc'];
	if($SATLOC == 'www' || '/www') {$SATLOC='';}
	define("SATLOC",$SATLOC);	
	}

$location = $_SERVER['DOCUMENT_ROOT']. SATLOC;
require_once($location . '/wp-load.php'); 
require_once($location . '/wp-config.php'); 
global $wpdb; 
global $level2;
global $wp_user;

$level2 		=	intval($_POST['level2']);
$level3 		=	intval($_POST['level3']);
$wp_user		=	($_POST['user_id']);
$talent			=	$_POST['talent'];


wp_enqueue_script('jquery');

/*** Primary Talent ***/
if($talent == 1) {

if($wp_user!=NULL){
	$save	=	$wpdb->update( 
		'sat_01_talent', 
		array( 'talent_id' => $level3 ),
		array( 'user_id' => $wp_user )
	);
$affected = intval($wpdb->rows_affected);
	if($affected > 0){
	echo '<span class="talent-saved"><img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" /></span>
	<meta value="'.$SATLOC.'" />';
	exit();
	}//end nested if	
}//end if

$rows = intval($wpdb->rows_affected);
//echo '<h1>Rows: ' . $rows . '</h1>';

if( $rows == 0 && $wp_user != 0) {
		$save = $wpdb->insert(
		'sat_01_talent', 
		array(
			'user_id' => $wp_user,
			'talent_id' => $level3
		));	
return '<span class="talent-saved"><img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" /></span>';
exit();

}
}// end if talent = 1

/*** 2nd Talent ***/
elseif($talent == 2) {
if($wp_user!=NULL){
	$save	=	$wpdb->update( 
		'sat_02_talent', 
		array( 'talent_id' => $level3 ),
		array( 'user_id' => $wp_user )
	);
$affected = intval($wpdb->rows_affected);
	if($affected > 0){
	echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
	exit();
	}//end nested if	
}//end if

$rows = intval($wpdb->rows_affected);
//echo '<h1>Rows: ' . $rows . '</h1>';

if( $rows == 0 && $wp_user != 0) {
		$save = $wpdb->insert(
		'sat_02_talent', 
		array(
			'user_id' => $wp_user,
			'talent_id' => $level3
		));	
echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
exit();

}
}// end if talent = 2

/*** 3rd Talent ***/
elseif($talent == 3) {
if($wp_user!=NULL){
	$save	=	$wpdb->update( 
		'sat_03_talent', 
		array( 'talent_id' => $level3 ),
		array( 'user_id' => $wp_user )
	);
$affected = intval($wpdb->rows_affected);
	if($affected > 0){
	echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
	exit();
	}//end nested if	
}//end if

$rows = intval($wpdb->rows_affected);
//echo '<h1>Rows: ' . $rows . '</h1>';

if( $rows == 0 && $wp_user != 0) {
		$save = $wpdb->insert(
		'sat_03_talent', 
		array(
			'user_id' => $wp_user,
			'talent_id' => $level3
		));	
echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
exit();

}
}// end if talent = 3

/*** 4th Talent ***/
elseif($talent == 4) {
if($wp_user!=NULL){
	$save	=	$wpdb->update( 
		'sat_04_talent', 
		array( 'talent_id' => $level3 ),
		array( 'user_id' => $wp_user )
	);
$affected = intval($wpdb->rows_affected);
	if($affected > 0){
	echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
	exit();
	}//end nested if	
}//end if

$rows = intval($wpdb->rows_affected);
//echo '<h1>Rows: ' . $rows . '</h1>';

if( $rows == 0 && $wp_user != 0) {
		$save = $wpdb->insert(
		'sat_04_talent', 
		array(
			'user_id' => $wp_user,
			'talent_id' => $level3
		));	
echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
exit();

}
}// end if talent = 4

/*** 5th Talent ***/
elseif($talent == 5) {
if($wp_user!=NULL){
	$save	=	$wpdb->update( 
		'sat_05_talent', 
		array( 'talent_id' => $level3 ),
		array( 'user_id' => $wp_user )
	);
$affected = intval($wpdb->rows_affected);
	if($affected > 0){
	echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
	exit();
	}//end nested if	
}//end if

$rows = intval($wpdb->rows_affected);
//echo '<h1>Rows: ' . $rows . '</h1>';

if( $rows == 0 && $wp_user != 0) {
		$save = $wpdb->insert(
		'sat_05_talent', 
		array(
			'user_id' => $wp_user,
			'talent_id' => $level3
		));	
echo '<span class="talent-saved">
		<img src="' . get_stylesheet_directory_uri(). '/images/check-mark.png" />
		</span>';
exit();

}
}// end if talent = 5


?>