<?php

class JSON_API_Talent_Controller {

public function talent_top_level_api() {
global $json_api;
global $wpdb;
global $bp;

$userid 	=	mysql_escape_string($_GET['userid']);
$level		=	mysql_escape_string($_GET['level']);

//echo '<p>Results</p>';

$talent = $wpdb->get_results( 
	"
	SELECT talent_id as talentid
	FROM `sat_01_talent`
	where user_id = $userid
	"
);

$talentid = $talent[0]->talentid;

$talent_name = $wpdb->get_results( 
	"
	SELECT *
	FROM `sat_talents`
	where id = $talentid
	"
);

	$top_level_option 	= $talent_name[0]->top_level;
	$sec_level_option 	= $talent_name[0]->level_2;
	$name_level_option	= $talent_name[0]->level_3;
	$talent_obj	= array(
		"top" => $top_level_option,
		"sec" => $sec_level_option,
		"nam" => $name_level_option
	);
//$talent_json = json_encode($talent_obj);
return array(
	"status" => "ok",
	"top" => $talent_obj
);
		
		
  }// end function

/*******************************/
/******  2nd Level Talent ******/
/*******************************/

public function talent_second_level(){
global $json_api;
global $wpdb;
global $bp;
$userid 	=	mysql_escape_string($_GET['userid']);
$top		=	mysql_escape_string($_GET['top']);
	
$level_2_obj = $wpdb->get_results( 
	"
	SELECT DISTINCT level_2
	FROM `sat_talents`
	where top_level = '$top'
	"
);

$option = array();
$n=0;
foreach($level_2_obj as $l){
$option[$n] 	=	array(
	"category"	=> "$l->level_2"
	);	
$n++;
}


	return array(
		"status" => "ok",
		"second" => $option
	);
	
}// end function

/********************************/
/******  Name Level Talent ******/
/********************************/

public function talent_name_level(){
	
global $wpdb; 
global $top_level;
global $level_2;
$userid 	=	mysql_escape_string($_GET['userid']);
$cat		=	mysql_escape_string($_GET['cat']);

$level_3_obj = $wpdb->get_results( 
	"
	SELECT *
	FROM `sat_talents`
	where level_2 = '$cat'
	"
);

$option = array();
$n=0;
foreach($level_3_obj as $l){
$option[$n] 	=	array(
	"name"	=> "$l->level_3",
	"id"	=> "$l->id"
	);	
$n++;
}

$list = array(
"status"	=> "ok",
"list" 		=> $option
);


//$level_3_json = json_encode($list);

return array(
	"status" 	=> "ok",
	"name"		=> $option
);

} // end function

/****************************/
/******  Search Talent ******/
/****************************/
  public function searchTalent_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= mysql_escape_string($_GET['userid']);
		$talent		= mysql_escape_string($_GET['talent']);

return array(
	"user" => $userid ,
	"talent" => $talent
);

		
  }// end function

/****************************/
/******  Search Talent ******/
/****************************/
  public function searchMembers_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 		= mysql_escape_string($_GET['userid']);
		$search			= mysql_escape_string($_GET['search']);
		$search_clean	= str_replace("@","",$search);

$search_results = $wpdb->get_results("
SELECT ID as id, user_nicename 
FROM  `wp_users` 
WHERE user_nicename LIKE  '$search_clean%'
");

$n=0;
foreach ($search_results as $s) {
	$search_talent_id = $s->id;
	$search_talent = $wpdb->get_results("
	SELECT talent_id as talentid from `sat_01_talent` WHERE user_id = $search_talent_id LIMIT 1;
	");
	$talent_id = $search_talent[0]->talentid;
	if(!$talent_id){$talent_id = 0;}	
	$n++;	
// if has talent, get talent name and assign type 1
	if($talent_id>0) {
		$search_talent_name = $wpdb->get_results("
		SELECT * from `sat_talents` WHERE id = $talent_id LIMIT 1;
		");		
		$user_results_id[$n] = array(
		"userid" 	=> $s->id,
		"type"		=> "1",
		"name"		=> $s->user_nicename,
		"talentid"	=> $talent_id,
		"talent_top"=> $search_talent_name[0]->top_level,
		"talent_sec"=> $search_talent_name[0]->level_2,
		"talent_nam"=> $search_talent_name[0]->level_3,
		);
	} // end if

// if has no talent, assign "no talent selected" and assign type 0
		if($talent_id==0) {			
		$user_results_id[$n] = array(
		"userid" 	=> $s->id,
		"type"		=> "0",
		"name"		=> $s->user_nicename,
		"talentid"	=> $talent_id,
		"talent_top"=> "No Talent Selected",
		"talent_sec"=> "",
		"talent_nam"=> ""
		);
	} // end if
	
}// end foreach
$search_results_clean = array();
$orderby = "type";
array_multisort($search_results_clean[$orderby],SORT_DESC,$user_results_id);

return array(
	"user" => $userid ,
	"results" => $user_results_id,
	"results_c" => $search_results_clean
);

		
  }// end function

}// end class
?>