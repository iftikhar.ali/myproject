<?php

class JSON_API_userSettings_Controller {

	public function userSettings_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= $_GET['userid'];
$r=0;

// Zip Code		= 3
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

if(!$userid){
	$status = "error";
	$message = "We cannot find a User ID in your request. Check your formatting and try again.";
	$errorcode = 1;
	}
else {
	$status = "ok";
	$message = "User Found.";
	$errorcode = 0;
	}		
		
$zip = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 3");

$parent = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 4");	

$country = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 42");

$month = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 46");

$year = $wpdb->get_results("SELECT value 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 59");

$settings_obj = array(
	"zip"	=> $settings[4]
);

return array(
			"status" 		=> $status,
			"message"		=> $message,
			"errocode"		=> $errorcode,			
			"zip" 			=>$zip[0]->value,
			"parent"		=>$parent[0]->value,
			"country"		=>$country[0]->value,
			"month"			=>$month[0]->value,
			"year"			=>$year[0]->value
	    );			
		
  }// end function

/*****************************/
/******** UPDATE USER ********/
/*****************************/

public function userSettings_update_api() {
global $json_api;
global $wpdb;
global $bp;
$userid 	= $_GET['userid'];
$zip		= $_GET['zip'];
$parent		= $_GET['parent'];
$country	= $_GET['country'];
$month		= $_GET['month'];
$year		= $_GET['year'];


// POSTAL CODE UPDATE
$zip_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 3");
$zipcode = $zip_check[0]->value;
$zip_id	= $zip_check[0]->id;

if($zipcode == NULL){
	$zip_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '3',  '$userid',  '$zip',  ''
		);
	");
}// end if
elseif($zipcode > 0) {
	$zip_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$zip' WHERE  `wp_bp_xprofile_data`.`id` = $zip_id LIMIT 1;	
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// PARENT UPDATE
$parent_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 4");
$parent_result = $parent_check[0]->value;
$parent_id	= $parent_check[0]->id;

if($parent_result == NULL){
	$parent_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '4',  '$userid',  '$parent',  ''
		);
	");
}// end if
elseif($parent != NULL) {
	$parent_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$parent' WHERE  `wp_bp_xprofile_data`.`id` = $parent_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// COUNTRY UPDATE
$country_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 42");
$country_result = $country_check[0]->value;
$country_id	= $country_check[0]->id;

if($country_result == NULL){
	$country_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '42',  '$userid',  '$country',  ''
		);
	");
}// end if
elseif($country_check != NULL) {
	$country_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$country' WHERE  `wp_bp_xprofile_data`.`id` = $country_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// YEAR UPDATE
$year_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 59");
$year_result = $year_check[0]->value;
$year_id	= $year_check[0]->id;

if($year_check == NULL){
	$year_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '59',  '$userid',  '$year',  ''
		);
	");
}// end if
elseif($year_check != NULL) {
	$year_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$year' WHERE  `wp_bp_xprofile_data`.`id` = $year_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// MONTH UPDATE
$month_check = $wpdb->get_results("SELECT * 
FROM  `wp_bp_xprofile_data` 
WHERE user_id = $userid AND field_id = 46");
$month_result = $month_check[0]->value;
$month_id	= $month_check[0]->id;

if($month_check == NULL){
	$month_update = $wpdb->get_results("
		INSERT INTO  `wp_bp_xprofile_data` (
			`id`,
			`field_id` ,
			`user_id` ,
			`value` ,
			`last_updated`
		)
		VALUES (
			NULL ,  '46',  '$userid',  '$month',  ''
		);
	");
}// end if
elseif($month_check != NULL) {
	$month_update = $wpdb->get_results("
	UPDATE  `wp_bp_xprofile_data` SET `value` =  '$month' WHERE  `wp_bp_xprofile_data`.`id` = $month_id LIMIT 1;
	");
	$r =  $r + intval($wpdb->rows_affected);
}// end elseif

// Zip Code		= 3
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

if(!$userid){
	$status = "error";
	$message = "We cannot find a User ID in your request. Check your formatting and try again.";
	$errorcode = 1;
	}
else {
	$status = "ok";
	$message = '<span style="color:red;">Settings Updated!</span>';
	$errorcode = 0;
	}		
		
return array(
			"status" 		=> $status,
			"message"		=> $message,
			"errocode"		=> $errorcode,			
			"zip" 			=> $zip,
			"parent"		=> $parent,
			"country"		=> $country,
			"month"			=> $month,
			"year"			=> $year,
			"query"			=> $wpdb->last_query,
			"rows"			=> $wpdb->rows_affected
	    );			
		
  }// end function
  
}// end class
?>