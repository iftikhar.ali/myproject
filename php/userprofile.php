<?php

class JSON_API_userProfile_Controller {

  public function userProfile_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= $_GET['userid'];

// Zip Code		= 36
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

		if(!$userid){
			$status = "error";
			$message = "We cannot find a User ID in your request. Check your formatting and try again.";
			$errorcode = 1;
			}
		else {
			$status = "ok";
			$message = "User Found.";
			$errorcode = 0;
			}
		$gravatar 	= get_avatar($userid,450);		
		$user_info = get_userdata($userid);
		//var_dump($user_info);

		if($user_info == false) {
			$status = "error";
			$message = "We cannot find a user for the User ID in your request.";
			$errorcode = 2;			
			}
		$name = $user_info->user_login;

// Talent ID lookup
		$talent_id = $wpdb->get_results("
		SELECT talent_id as tid
		FROM sat_01_talent
		WHERE  `user_id` = $userid 
		LIMIT 1
		");
		$talent_1_id = $talent_id[0]->tid;

// Talent name lookup 
		$talent = $wpdb->get_results("
		SELECT level_3 as talent
		FROM  `sat_talents` 
		WHERE id = $talent_1_id
		LIMIT 1
		");
		$talent_1_name = $talent[0]->talent;

// Talent detail lookup 
		$talent_detail = $wpdb->get_results("
		SELECT * 
		FROM  `sat_talents` 
		WHERE id = $talent_1_id
		LIMIT 1
		");

// About talent lookup
		$talent_about = $wpdb->get_results("
		SELECT about_talent as about
		FROM sat_about_talent
		WHERE  `user_id` = $userid 
		LIMIT 1
		");
		$talent_1_about = $talent_about[0]->about;

// Postal code lookup
		$postal_code_look = $wpdb->get_results("
		SELECT value
		FROM  `wp_bp_xprofile_data` 
		WHERE user_id = $userid
		AND field_id = 36
		");
		$postal_code = $postal_code_look[0]->value;
		//var_dump($postal_code_look);

// Parent lookup
		$parent_look = $wpdb->get_results("
		SELECT value
		FROM  `wp_bp_xprofile_data` 
		WHERE user_id = $userid
		AND field_id = 4
		");
		$parent = $parent_look[0]->value;

// Activation Type lookup
		$actType_look = $wpdb->get_results("
				SELECT * 
				FROM  `sat_hash` 
				where user_id = $userid
				");
				$act_type = intval($actType_look[0]->status);
// Activity Counter
$activity_count = bp_get_activity_count($userid);
// Friend Counter
$friend_count	= BP_Friends_Friendship::total_friend_count( $userid );
// Friend Request Counter
$friend_request_count = bp_friend_get_total_requests_count($userid);
// Endorsement Counter 
$endoresment = $wpdb->get_results("
SELECT * 
FROM  `sat_endorse` 
WHERE user_id = $userid
");
$endoresment_count = $wpdb->num_rows;

$html_panel = '
<!-- SETTINGS -->
        	    
<!-- /settings -->

';

$html_content = '
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" href="img/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.css" />
<script src="css/jquery-1.10.2.js"></script>
<script src="css/jquery.mobile-1.3.2.js"></script>

 <!-- CONTENT -->
<div id="profile-content">
	<div id="profile-header">
		<div id="gravatar">'. $gravatar .'</div>
		<div id="name-talent-highlight">
			<h2>'.$user_info->display_name.'</h2>
			<h4>'.$talent_detail[0]->top_level.'->'.$talent_detail[0]->level_2.'->'.$talent_detail[0]->level_3.'</h4>		
		</div>
	</div>

	<div id="profile-options">
		<ul data-role="listview" data-inset="true">
			<li>Endorsements <span class="counter">('.$endoresment_count.')</span></li>
			<li><a href="#">Recent Activity <span class="counter">('.$activity_count.')</span></a></li>
			<li><a href="#">Friends <span class="counter">('.$friend_count.')</span></a></li>
			<li><a href="#">Friend Requests <span class="counter">('.$friend_request_count.')</span></a></li>
			<li><a href="#">Groups</a></li>
		</ul>
	</div>
	<div id="profile-about-talent">
		<p>'.$talent_1_about.'</p>
	</div>
</div>
    <!-- /profile-content -->
';

// User Object		
	$user_obj = array(
			"displayname" 		=> $user_info->display_name,
			"login" 			=> $user_info->user_login,
			"email"				=> $user_info->user_email,
			"postalcode" 		=> $postal_code,
			"talent_1_name"		=> $talent_1_name,
			"talent_1_id"		=> $talent_1_id,	
			"talent_1_about"	=> $talent_1_about,
			"talent_1_top"		=> $talent_detail[0]->top_level,
			"talent_1_lv2"		=> $talent_detail[0]->level_2,
			"parent"			=> $parent,
			"gravatar"			=> $gravatar,
			"endoresment_count"	=> $endoresment_count,
			"activity_count"	=> $activity_count,
			"friend_count" 		=> $friend_count,
			"friend_request_count"	=> $friend_request_count,
			"type"				=> $act_type,
			);
		
		return array(
			"status" 	=> $status,
			"message"	=> $message,
			"errocode"	=> $errorcode,
			"user"		=> $user_obj,
			"html"		=> $html_content,
			"html_panel"=>	$html_panel
	    );	
		
  }// end function
/************************************************/


public function memberId_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= $_GET['memberid'];
		$me			= $_GET['userid'];

// Zip Code		= 36
// Parent  		= 4
// Country		= 42
// Birth Month 	= 46
// Birth Year	= 59

		if(!$userid){
			$status = "error";
			$message = "We cannot find a User ID in your request. Check your formatting and try again.";
			$errorcode = 1;
			}
		else {
			$status = "ok";
			$message = "User Found.";
			$errorcode = 0;
			}
$gravatar 	= get_avatar($userid,450);		
$user_info = get_userdata($userid);
//var_dump($user_info);

if($user_info == false) {
	$status = "error";
	$message = "We cannot find a user for the User ID in your request.";
	$errorcode = 2;			
	}
$name = $user_info->user_login;

// Are we friends?
$friend_check = $wpdb->get_results("
	SELECT *
	FROM  `wp_bp_friends` 
	WHERE  `initiator_user_id` = $me
	AND  `friend_user_id` = $userid
	LIMIT 1
");

$friend_check2 = $wpdb->get_results("
	SELECT *
	FROM  `wp_bp_friends` 
	WHERE  `initiator_user_id` = $userid
	AND  `friend_user_id` = $me	
	LIMIT 1
");

$fc1 = $friend_check[0]->id;
$fc2 = $friend_check2[0]->id; 
$fc1_status = $friend_check[0]->is_confirmed;
$fc2_status = $friend_check2[0]->is_confirmed;

// set friend_check
if($fc1) {	
	$fc = $fc1;
}
elseif($fc2) {
	$fc = $fc2;
	}
else {$fc=0; end;} 


// set friend_status
if($fc1_status == NULL){
$fs = $fc2_status;	
$init = $friend_check2[0]->initiator_user_id;
}else {$fs = $fc1_status;$init = $friend_check[0]->initiator_user_id;}


// Talent ID lookup
		$talent_id = $wpdb->get_results("
		SELECT talent_id as tid
		FROM sat_01_talent
		WHERE  `user_id` = $userid 
		LIMIT 1
		");
		$talent_1_id = $talent_id[0]->tid;

// Talent name lookup 
		$talent = $wpdb->get_results("
		SELECT level_3 as talent
		FROM  `sat_talents` 
		WHERE id = $talent_1_id
		LIMIT 1
		");
		$talent_1_name = $talent[0]->talent;

// Talent detail lookup 
		$talent_detail = $wpdb->get_results("
		SELECT * 
		FROM  `sat_talents` 
		WHERE id = $talent_1_id
		LIMIT 1
		");

// About talent lookup
		$talent_about = $wpdb->get_results("
		SELECT about_talent as about
		FROM sat_about_talent
		WHERE  `user_id` = $userid 
		LIMIT 1
		");
		$talent_1_about = $talent_about[0]->about;

// Postal code lookup
		$postal_code_look = $wpdb->get_results("
		SELECT value
		FROM  `wp_bp_xprofile_data` 
		WHERE user_id = $userid
		AND field_id = 36
		");
		$postal_code = $postal_code_look[0]->value;
		//var_dump($postal_code_look);

// Parent lookup
		$parent_look = $wpdb->get_results("
		SELECT value
		FROM  `wp_bp_xprofile_data` 
		WHERE user_id = $userid
		AND field_id = 4
		");
		$parent = $parent_look[0]->value;

// Activity Counter
$activity_count = bp_get_activity_count($userid);
// Friend Counter
$friend_count	= BP_Friends_Friendship::total_friend_count( $userid );
// Friend Request Counter
$friend_request_count = bp_friend_get_total_requests_count($userid);
// Endorsement Counter 
$endoresment = $wpdb->get_results("
SELECT * 
FROM  `sat_endorse` 
WHERE user_id = $userid
");
$endoresment_count = $wpdb->num_rows;

$html_panel = '
<!-- SETTINGS -->
        	    
<!-- /settings -->

';

$html_content = '
<link rel="stylesheet" type="text/css" href="css/index.css" />
<link rel="stylesheet" href="img/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery.mobile-1.3.2.css" />
<script src="css/jquery-1.10.2.js"></script>
<script src="css/jquery.mobile-1.3.2.js"></script>

 <!-- CONTENT -->
<div id="profile-content">
	<div id="profile-header">
		<div id="gravatar">'. $gravatar .'</div>
		<div id="name-talent-highlight">
			<h2>'.$user_info->display_name.'</h2>
			<h4>'.$talent_detail[0]->top_level.'->'.$talent_detail[0]->level_2.'->'.$talent_detail[0]->level_3.'</h4>		
		</div>
	</div>

	<div id="profile-options">
		<ul data-role="listview" data-inset="true">
			<li>Endorsements <span class="counter">('.$endoresment_count.')</span></li>
			<li><a href="#">Recent Activity <span class="counter">('.$activity_count.')</span></a></li>
			<li><a href="#">Friends <span class="counter">('.$friend_count.')</span></a></li>
			<li><a href="#">Friend Requests <span class="counter">('.$friend_request_count.')</span></a></li>
			<li><a href="#">Groups</a></li>
		</ul>
	</div>
	<div id="profile-about-talent">
		<p>'.$talent_1_about.'</p>
	</div>
</div>
    <!-- /profile-content -->
';

// User Object		
	$user_obj = array(
			"displayname" 		=> $user_info->display_name,
			"login" 			=> $user_info->user_login,
			"email"				=> $user_info->user_email,
			"postalcode" 		=> $postal_code,
			"talent_1_name"		=> $talent_1_name,
			"talent_1_id"		=> $talent_1_id,	
			"talent_1_about"	=> $talent_1_about,
			"talent_1_top"		=> $talent_detail[0]->top_level,
			"talent_1_lv2"		=> $talent_detail[0]->level_2,
			"parent"			=> $parent,
			"gravatar"			=> $gravatar,
			"endoresment_count"	=> $endoresment_count,
			"activity_count"	=> $activity_count,
			"friend_count" 		=> $friend_count,
			"friend_request_count"	=> $friend_request_count,
			"friend_check"		=> $fc,
			"friend_status"		=> $fs,
			"initiator"			=> $init,
			"debug"				=> $fc1_status.' | '.$fc2_status,
			);
		
		return array(
			"status" 	=> $status,
			"message"	=> $message,
			"errocode"	=> $errorcode,
			"user"		=> $user_obj,
			"html"		=> $html_content,
			"html_panel"=>	$html_panel
	    );	
		
  }// end function
/************************************************/  

public function childAccounts_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= $_GET['userid'];

$find_child_accounts = $wpdb->get_results("
SELECT user_id 
FROM  `wp_usermeta` 
WHERE meta_key =  'st_parent_id'
AND meta_value = $userid
LIMIT 5
");

$n=0;
while($n<6) {
	$child_id = $find_child_accounts[$n]->user_id;
	if($child_id){
		$user_info = get_userdata($child_id);
		$is_active = $wpdb->get_results("
			SELECT meta_value 
			FROM  `wp_usermeta` 
			WHERE meta_key =  'st_active_child'
			AND user_id = $child_id
			LIMIT 1
		");
		$active = $is_active[0]->meta_value;
		//var_dump($is_active);
		$child_details[$n] = array(
			"childID" 		=> $user_info->ID,
			"childLogin" 	=> $user_info->user_login,
			"childDisplay"	=> $user_info->display_name,
			"active"		=> $active
			);
		}
	$n++;
}// end while

return array(
"status" 	=> "ok",
"details" 	=> $child_details
);
		
  }// end function
/************************************************/  

public function childAccountsToggle_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= $_GET['userid'];
		$state		= $_GET['state'];

$locked_previously = $wpdb->get_results("
SELECT *
FROM  `wp_usermeta` 
WHERE user_id = $userid
AND meta_key = 'wp_ul_locked'
");

if(!$locked_previously) {
	$lock = $wpdb->get_results("
		INSERT INTO  `sha1306407571361`.`wp_usermeta` (
		`umeta_id` ,
		`user_id` ,
		`meta_key` ,
		`meta_value`
		)
		VALUES (
		NULL ,  '$userid',  'wp_ul_locked',  '1'
		);
		");
	}

elseif($locked_previously) {
if($state == 0){
$wpdb->update( 
	'wp_usermeta', 
	array( 	
		'meta_value' => 0 
		), 
	array( 
		'user_id' => $userid,
		'meta_key' => 'wp_ul_locked'
	)
);

return array(
	"status" 	=> "ok",
	"message" 	=> "Child Account Enabled"
);
}// end if state == 1

if($state == 1){
$wpdb->update( 
	'wp_usermeta', 
	array( 	
		'meta_value' => 1 
		), 
	array( 
		'user_id' => $userid,
		'meta_key' => 'wp_ul_locked'
	)
);
}
return array(
	"status" 	=> "ok",
	"message" 	=> "Child Account Disabled"
);
}// end if state == 0
		
  }// end function
/************************************************/  

public function childAccountsChangePassword_api() {
		global $json_api;
		global $wpdb;
		global $bp;
		$userid 	= $_GET['userid'];
		$pass		= $_GET['pass'];
$pass_hashed = wp_hash_password($pass);
$set_pass = $wpdb->get_results("
UPDATE  `wp_users` 
SET  `user_pass` =  '$pass_hashed' 
WHERE `ID` = $userid 
LIMIT 1 ;
");

return array(
	"status" 	=> "ok",
	"message" 	=> "Password Changed"
);
		
  }// end function
/************************************************/  

public function userActivities_api() {
	global $json_api;
	global $wpdb;
	global $bp;
	$userid 	= $_GET['userid'];
	$r=0;

$activities = $wpdb->get_results("
	SELECT * 
	FROM  `wp_bp_activity` 
	WHERE user_id = $userid
	AND component =  'activity'
	AND (	
	type =  'activity_update'
	OR type = 'rtmedia_update'
	)
");
$comments = array();
foreach($activities as $a){
	$aid = $a->id;
	$comment = $wpdb->get_results("
	SELECT * 
FROM  `wp_bp_activity` 
WHERE component =  'activity'
AND TYPE =  'activity_comment'
AND item_id = $aid
	");
	if($comment){
	$comments[$aid] = $comment;
	}
}
/*
$comments = $wpdb->get_results("
	SELECT * 
	FROM  `wp_bp_activity` 
	WHERE user_id = $userid
	AND component =  'activity'
	AND type =  'activity_comment'
");
*/
//$comments_message = $wpdb->last_query;

if($activities == NULL) {return array(
	"status" => "error",
	"message" => "No activities found...yet!",
	"userid" => $userid,
);}// end if
else {

//echo '<p>'.$comments_message.'</p>';
return array(
	"status" 		=> "ok",
	"userid" 		=> $userid,
	"activities" 	=> $activities,
	"comments" 		=> $comments
);
}// end else



  }// end function
/************************************************/  
  
}// end class

?>